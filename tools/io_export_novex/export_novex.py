
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy

def faceToTriangles(face, idx, ent):
    triangles = []
    if len(face) == 4:
        triangles.append([face[0], face[1], face[2], idx, ent])
        triangles.append([face[2], face[3], face[0], idx, ent])
    else:
        triangles.append(face + [idx, ent])

    return triangles

def faceValues(face, mesh, matrix):
    fv = []

    for i, verti in enumerate(face.vertices):
        uv_layer = mesh.uv_layers.active.data

        fv.append((matrix * mesh.vertices[verti].co)[:] + uv_layer[face.loop_indices[i]].uv[:])

    return fv

def faceToLine(face):
    return " ".join([("%.6f %.6f %.6f %.6f %.6f" % v) for v in face[:3]] + [" "] + [str(face[3]), " ", str(face[4])]) + "\n"

def write(filepath, applyMods=True):
    scene = bpy.context.scene

    faces = []
    mats = []

    objs = [bpy.context.active_object]

    entities = []

    for obj in bpy.context.scene.objects:
        if obj.novex.classname == 'none':
            continue
        ent = -1
        if obj.novex.classname != "none":
            if obj.type in ["EMPTY", "MESH"]:
                if obj.novex.classname != 'map':
                    entities.append([obj.location[0],
                                     obj.location[1],
                                     obj.location[2],
                                     obj.rotation_euler[2],
                                     obj.novex.classname,
                                     obj.novex.target,
                                     int(obj.novex.startbackwards),
                                     int(obj.novex.remote),
                                     float(obj.novex.customvars[0]),
                                     float(obj.novex.customvars[1]),
                                     float(obj.novex.customvars[2]),
                                     float(obj.novex.customvars[3]),
                                     obj.novex.lock,
                                     obj
                    ])
                    if obj.type == "EMPTY":
                        continue
                    ent = len(entities) - 1
        if applyMods or obj.type != "MESH":
            try:
                me = obj.to_mesh(scene, True, "PREVIEW")
            except:
                me = None

            is_tmp_mesh = True
        else:
            me = obj.data

            if not me.tessfaces and me.polygons:
                me.calc_tessface()

            is_tmp_mesh = False

        if me is not None:
            matrix = obj.matrix_world.copy()

            for i, face in enumerate(me.polygons):
                mat = me.uv_textures[0].data[i].image
                fv = faceValues(face, me, matrix)
                mat_idx = 0

                if mat in mats:
                    mat_idx = mats.index(mat)
                else:
                    mat_idx = len(mats)
                    mats.append(mat)

                faces.extend(faceToTriangles(fv, mat_idx, ent))

            if is_tmp_mesh:
                bpy.data.meshes.remove(me)
    for ent in entities:
        trg = ent[5]
        ent[5] = -1
        for j in range(len(entities)):
            if entities[j][-1] == trg:
                ent[5] = j

    faces = sorted(faces, key = lambda a: a[3])

    # Write the faces to a file

    file = open(filepath, "w")

    file.write(str(len(mats)) + "\n")
    file.write(str(len(faces)) + "\n")
    file.write(str(len(entities)) + "\n")

    for mat in mats:
        file.write("tiles/" + mat.name.split("/")[-1] + "\n")

    for face in faces:
        file.write(faceToLine(face))

    for ent in entities:
        file.write("{} {} {} {} {} {} {} {} {} {} {} {} {}".format(*ent) + "\n")

    file.close()
