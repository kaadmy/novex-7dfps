
# This addon is a modified version of Blender's builtin io_mesh_raw exporter. Most of the code is from there.

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Novex mesh format (.nlv)",
    "author": "Anthony D, Agostino (Scorpius), Aurel Wildfellner, KaadmY, Literally Void",
    "version": (0, 2),
    "blender": (2, 79, 0),
    "location": "File > Import-Export > Novex Level (.nlv) ",
    "description": "Export Novex levels",
    "warning": "This export plugin is very much experimental! Use at your own risk",
    "category": "Import-Export",
}

if "bpy" in locals():
    import importlib

    if "export_novex" in locals():
        importlib.reload(export_novex)
else:
    import bpy

from bpy.props import StringProperty, BoolProperty
from bpy_extras.io_utils import ExportHelper

class NovexExporter(bpy.types.Operator, ExportHelper):
    bl_idname = "export_mesh.novex"
    bl_label = "Export Novex Level"

    filename_ext = ".nlv"
    filter_glob = StringProperty(default="*.nlv", options={"HIDDEN"})

    apply_modifiers = BoolProperty(
        name="Apply Modifiers",
        description="Use transformed mesh data from each object",
        default=True,
    )

    def execute(self, context):
        from . import export_novex

        export_novex.write(self.filepath, self.apply_modifiers)

        return {"FINISHED"}

class NovexCreateMaterial(bpy.types.Operator):
    bl_idname = "novex.create_materials"
    bl_label = "Preload Novex Materials"

    def execute(self, context):
        import os

        tiles = os.listdir(context.scene.novex.path)
        for tile in tiles:
            if not tile.endswith('png'):
                continue
            if tile in bpy.data.materials:
                mat = bpy.data.materials[tile]
            else:
                mat = bpy.data.materials.new(tile)
            if tile in bpy.data.textures:
                tex = bpy.data.textures[tile]
            else:
                tex = bpy.data.textures.new(tile, 'IMAGE')
            t = context.scene.novex.path + '/' + tile
            if t in bpy.data.images:
                img = bpy.data.images[t]
            else:
                img = bpy.data.images.load(t)
            tex.image = img
            try:
                mat.texture_slots[0].texture = tex
            except:
                mat.texture_slots.add().texture = tex
            mat.name = tile

        return {"FINISHED"}

class NovexSceneProps(bpy.types.PropertyGroup):
    path = bpy.props.StringProperty(
        name="",
        description="Path to Directory",
        default="",
        maxlen=1024,
        subtype='DIR_PATH')

class NovexScenePanel(bpy.types.Panel):
    bl_idname = "OBJECT_PT_my_panel"
    bl_label = "Novex"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "scene"
    bl_category = "Novex"

    def draw(self, context):
        layout = self.layout
        scn = context.scene
        col = layout.column(align=True)
        col.prop(scn.novex, "path", text="")
        col.operator('novex.create_materials')

class ObjectData(bpy.types.PropertyGroup):
    classname = bpy.props.EnumProperty(default = "none", items = (
        ("none", "None", "If an object has this classname, it will be ignored during export."),

        ("map", "map", "The map. You should only have one of these, but if you have more, it'll work."),

        ("player", "player", "The player. Exactly one is expected and required."),

        ("func_door", "func_door", "A door. Startbackwards makes the door act like a lift. cv0 = travel, cv1 = speed, cv2 = time (<=0 disables return, cv3 = crush (0 = no, 1 = crush, -1 = gravity)"),
        ("func_switch", "func_switch", "A switch. Can be either momentary or stay. cv0 = stay type (>0.5 is one time use)"),

        ("prop_barrel", "prop_barrel", ""),
        ("prop_lamppost", "prop_lamppost", ""),

        ("item_health", "item_health", "Gives 25 health upon pickup."),
        ("item_armor", "item_armor", "Gives 50 armor upon pickup."),

        ("item_shells", "item_shells", ""),
        ("item_bullets", "item_bullets", ""),
        ("item_rockets", "item_rockets", ""),

        ("item_shotgun", "item_shotgun", "Shotgun."),
        ("item_supershotgun", "item_supershotgun", "Super Shotgun."),
        ("item_minigun", "item_minigun", "Minigun."),
        ("item_rocketlauncher", "item_rocketlauncher", "Rocket Launcher."),

        ("item_key", "item_key", "Keycard. Can change color."),

        ("enemy_grunt", "enemy_grunt", ""),
        ("enemy_flinger", "enemy_flinger", ""),
        ("enemy_gunner", "enemy_gunner", ""),
        ("enemy_floateye", "enemy_floateye", ""),
        ("enemy_arachnobob", "enemy_arachnobob", ""),

        ("trigger_changelevel", "trigger_changelevel", "Change to a specified level. cv0 = episode, cv1 = map"),
        ("trigger_cylinder", "trigger_cylinder", "Send out a trigger when an object touches this. cv0 = radius, cv1 = height, cv2 = delay, cv3 = player only"),
        ("trigger_teleport_destination", "trigger_teleport_destination", "Teleports the activator to this object when triggered."),

        ("item_collectible", "item_collectible", "A secret legendary helmet collectible."),

        ("trigger_counter", "trigger_counter", "Counter that triggers its target after it's triggered x times. cv0 = times required to trigger targets"),
))

    target = bpy.props.PointerProperty(type = bpy.types.Object)

    lock = bpy.props.EnumProperty(default = "0", items = (
        ("0", "No lock", ""),
        ("1", "Red key", ""),
        ("2", "Yellow key", ""),
        ("3", "Blue key", "")
    ))

    startbackwards = bpy.props.BoolProperty(default = False)
    remote = bpy.props.BoolProperty(default = False)

    customvars = bpy.props.FloatVectorProperty(default = [0, 0, 0, 0], size = 4)


class ObjectDataPanel(bpy.types.Panel):
    bl_label = "Novex Object Data"
    bl_idname = "novex.objdata_panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    bl_category = "Novex"

    def draw(self, context):
        if context.active_object.novex.classname == 'func_door':
            self.layout.label(context.active_object.novex.classname)
        self.layout.prop(context.active_object.novex, "classname")

        self.layout.prop(context.active_object.novex, "target")

        self.layout.prop(context.active_object.novex, "lock")

        self.layout.prop(context.active_object.novex, "startbackwards")
        self.layout.prop(context.active_object.novex, "remote")

        self.layout.column().prop(context.active_object.novex, "customvars")

def menu_export(self, context):
    self.layout.operator(NovexExporter.bl_idname, text="Novex Level Export")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_export)

    bpy.types.Object.novex = bpy.props.PointerProperty(type = ObjectData)
    bpy.types.Scene.novex = bpy.props.PointerProperty(type = NovexSceneProps)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_export)

if __name__ == "__main__":
    register()
