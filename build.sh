#! /bin/sh

alias COPY="cp ../../data/ . -r"

mkdir target
cd target
mkdir windows
mkdir linux

# LINUX BUILD
pushd linux
mkdir build
cd build
cmake ../../../
make
cd ..
cp build/novex-7dfps .
COPY
cp ../../CREDITS.txt .
cp ../../README.md .
rm -rf build
popd

# WINDOWS BUILD
pushd windows
mkdir build
cd build
i686-w64-mingw32-cmake ../../../
make
cd ..
cp ../../dlls/* .
cp build/novex-7dfps.exe .
COPY
cp ../../CREDITS.txt .
cp ../../README.md .
rm -rf build
popd

mv windows novex-7dfps
rm novex-7dfps-windows.zip
zip novex-7dfps-windows.zip -x '*.blend*' -x '*.aseprite*' -x '*.ase*' -r novex-7dfps/
rm -rf novex-7dfps
butler push novex-7dfps-windows.zip literallyvoid/novex-7dfps:windows

mv linux novex-7dfps
rm novex-7dfps-linux.zip
zip novex-7dfps-linux.zip -x '*.blend*' -x '*.aseprite*' -x '*.ase*' -r novex-7dfps/
rm -rf novex-7dfps
butler push novex-7dfps-linux.zip literallyvoid/novex-7dfps:linux
