
#include "local.h"

/* macros */

#define SIGN(a, b, c, a1, a2) (a[a1] - c[a1]) * (b[a2] - c[a2]) - (b[a1] - c[a1]) * (a[a2] - c[a2])

#define TRIANGLE_TYPE(triangle) triangle->normal[2] > 0.001 ? LEVEL_TRIANGLE_TYPE_FLOOR : (triangle->normal[2] < -0.001 ? LEVEL_TRIANGLE_TYPE_CEILING : LEVEL_TRIANGLE_TYPE_WALL)
#define TRIANGLE_IS_FLOOR(triangle) (triangle->normal[2] > 0.001)
#define TRIANGLE_IS_WALL(triangle) (TRIANGLE_TYPE(triangle) == LEVEL_TRIANGLE_TYPE_WALL)
#define TRIANGLE_IS_CEILING(triangle) (triangle->normal[2] < -0.001)

/* get if a point is in triangle */

static bool is_in_triangle(vec3_t pos, level_triangle_t *triangle, int a1, int a2) {
  bool b1 = SIGN(pos, triangle->vertices[0].pos, triangle->vertices[1].pos, a1, a2) < 0.0;
  bool b2 = SIGN(pos, triangle->vertices[1].pos, triangle->vertices[2].pos, a1, a2) < 0.0;
  bool b3 = SIGN(pos, triangle->vertices[2].pos, triangle->vertices[0].pos, a1, a2) < 0.0;

  return (b1 == b2) && (b2 == b3);
}

/* get height of a single triangle at point */

static vec_t triangle_height(level_triangle_t *triangle, vec3_t pos) {
  float iz = 1.0 / triangle->normal[2];
  float h = (-(triangle->normal[0] * pos[0] + triangle->normal[1] * pos[1]) + triangle->distance) * iz;
  return h;
}

static void calc_values(level_triangle_t *triangle) {
  // calculate face normal

  vec3_t a, b;

  UTIL_VEC3_SUB(a, triangle->vertices[1].pos, triangle->vertices[0].pos);
  UTIL_VEC3_SUB(b, triangle->vertices[2].pos, triangle->vertices[0].pos);

  vec3_t c = {a[1] * b[2] - a[2] * b[1],
              a[2] * b[0] - a[0] * b[2],
              a[0] * b[1] - a[1] * b[0]};

  UTIL_VEC3_COPY(triangle->normal, c);
  util_vec3_normalize(triangle->normal);
  triangle->distance = util_vec3_dot(triangle->normal, triangle->vertices[0].pos);
}

/* load level */

level_t *level_init(const char *path) {
  char path2[128] = DATA_PREFIX;
  strcat(path2, path);

  FILE *fp;

  if((fp = fopen(path2, "r")) == NULL) {
    printf("Couldn't open level file\n");
    return NULL;
  }

  int n_materials;
  if(fscanf(fp, "%d", &n_materials) != 1) {
    printf("Couldn't read level header\n");
    return NULL;
  }

  int n_triangles;
  if(fscanf(fp, "%d", &n_triangles) != 1) {
    printf("Couldn't read level header\n");
    return NULL;
  }

  int n_entities;
  if(fscanf(fp, "%d", &n_entities) != 1) {
    printf("Couldn't read level header\n");
    return NULL;
  }

  level_t *level = malloc(sizeof(level_t));

  level->n_triangles = n_triangles;
  level->n_materials = n_materials;
  level->triangles = malloc(sizeof(level_triangle_t*) * n_triangles);
  level->textures = malloc(sizeof(GLuint) * n_materials);
  level->materials = malloc(sizeof(level_material_t) * n_materials);

  // read in materials

  for(int i = 0; i < n_materials; i++) {
    char str[128];

    if(fscanf(fp, "%s", str) != 1) {
      printf("Couldn't read material\n");
      return NULL;
    }

    if(!strcmp(str, "tiles/nodraw")) {
      level->textures[i] = 0;
    } else {
      level->textures[i] = texture_load(str);
    }
    level_material_t mat;
    strcpy(mat.name, str);
    mat.fullbright = false;
    if(strstr(str, "light") != NULL) {
      mat.fullbright = true;
    }
    memcpy(&level->materials[i], &mat, sizeof(level_material_t));
  }

  // read in vertices

  for(int i = 0; i < n_triangles; i++) {
    level_triangle_t *triangle = malloc(sizeof(level_triangle_t));

    for(int j = 0; j < 3; j++) {
      if(fscanf(fp, "%f %f %f %f %f",
                &triangle->vertices[j].pos[0],
                &triangle->vertices[j].pos[1],
                &triangle->vertices[j].pos[2],
                &triangle->vertices[j].uv[0],
                &triangle->vertices[j].uv[1]) != 5) {
        printf("Couldn't read vertex\n");
        return NULL;
      }
      UTIL_VEC3_COPY(triangle->vertices[j].startpos, triangle->vertices[j].pos);
    }

    if(fscanf(fp, "%d %d", &triangle->material, &triangle->entity) != 2) {
      printf("Couldn't read vertex\n");
      return NULL;
    }

    triangle->doublesided = false;
    triangle->used = false;
    
    calc_values(triangle);
    if(triangle->normal[0] == 0.0 &&
       triangle->normal[1] == 0.0 &&
       triangle->normal[2] == 0.0) {
      printf("Triangle at %f %f %f is FAKE\n", triangle->vertices[0].pos[0], triangle->vertices[0].pos[1], triangle->vertices[0].pos[2]);
    }
    level->triangles[i] = triangle;
  }

  vec3_t ambient_color = {0.25, 0.23, 0.2};
  vec3_t sun_color = {1.0, 0.9, 0.7};
  vec3_t sun_direction = {0.2, 0.4, 0.7};

  UTIL_VEC3_COPY(level->ambient_color, ambient_color);
  UTIL_VEC3_COPY(level->sun_color, sun_color);
  UTIL_VEC3_COPY(level->sun_direction, sun_direction);

  util_vec3_normalize(level->sun_direction);

  level->n_entities = 0;
  level->alloc_entities = 1;
  level->entities = malloc(sizeof(entity_t*));

  int *targets = malloc(sizeof(int) * n_entities);

  for(int i = 0; i < n_entities; i++) {
    char classname[128];
    int target;
    int lock;
    vec3_t pos;
    vec_t angle;
    int startbackwards, remote;
    vec4_t vars;

    if(fscanf(fp, "%f %f %f %f %s %d %d %d %f %f %f %f %d", &pos[0], &pos[1], &pos[2], &angle, classname, &target, &startbackwards, &remote, &vars[0], &vars[1], &vars[2], &vars[3], &lock) != 13) {
      printf("Couldn't read entity\n");
      return NULL;
    }
    targets[i] = target;

    entity_t *ent = entity_init(level, classname, pos, angle, startbackwards, remote, vars);

    ent->lock = lock;

    level_add_entity(level, ent);

    if(!strcmp(classname, "player")) {
      level->player = ent;
    }
  }
  for(int i = 0; i < n_entities; i++) {
    if(targets[i] != -1) {
      level->entities[i]->target = level->entities[targets[i]];
    }
  }

  for(int i = 0; i < n_triangles; i++) {
    if(level->triangles[i]->entity >= 0) {
      entity_t *ent = level->entities[level->triangles[i]->entity];
      if((ent->n_triangles & (ent->n_triangles - 1)) == 0) {
        int n = (ent->n_triangles * 2);
        if(n == 0) n = 1;
        ent->triangles = realloc(ent->triangles, sizeof(level_triangle_t) * n);
      }
      ent->triangles[ent->n_triangles] = level->triangles[i];
      ent->n_triangles++;
      level->triangles[i]->entity_handle = ent;
    }
  }

  level->sky_texture = texture_load("tiles/sky2.png");

  return level;
}

/* free loaded level */

void level_free(level_t *level) {
  if(level == NULL) {
    return;
  }
  for(int i = 0; i < level->n_entities; i++) {
    entity_free(level->entities[i]);
  }
  free(level->entities);
  free(level->triangles);
  free(level->textures);
  free(level);
}

/* update level entities */

void level_add_entity(level_t *level, entity_t *e) {
  level->n_entities++;
  if(level->n_entities >= level->alloc_entities) {
    level->alloc_entities *= 2;
    level->entities = realloc(level->entities, sizeof(entity_t*) * level->alloc_entities);
  }
  level->entities[level->n_entities - 1] = e;
  if(e->onspawn != NULL) {
    e->onspawn(e);
  }
}

static void _level_remove_entity(level_t *level, entity_t *e) {
  for(int i = 0; i < level->n_entities; i++) {
    if(level->entities[i] == e) {
      entity_free(level->entities[i]);
      memmove(&level->entities[i], &level->entities[i + 1], sizeof(entity_t*) * (level->n_entities - (i + 1)));
      level->n_entities--;
      break;
    }
  }
}
void level_remove_entity(level_t *level, entity_t *e) {
  e->remove = true;
}

void level_update(level_t *level) {
  for(int i = 0; i < level->n_entities; i++) {
    entity_update(level->entities[i]);
  }
  for(int i = 0; i < level->n_entities; i++) {
    if(level->entities[i]->remove) {
      _level_remove_entity(level, level->entities[i]);
      i--;
      continue;
    }
  }
  for(int i = 0; i < level->n_triangles; i++) {
    if(level->triangles[i]->entity != -1) {
      calc_values(level->triangles[i]);
    }
  }
  for(int i = 0; i < level->n_entities - 1; i++) {
    if(level->entities[i]->collision_type == ENT_COLLISION_TYPE_NONE) {
      continue;
    }
    if(level->entities[i]->collision_type == ENT_COLLISION_TYPE_CORPSE) {
      continue;
    }
    for(int j = i + 1; j < level->n_entities; j++) {
      entity_collide(level->entities[i], level->entities[j]);
    }
  }
}

/* main rendering (called from renderer_render) */

void level_render(level_t *level) {
  int tex = -1;
  glDisable(GL_CULL_FACE);
  for(int i = 0; i < level->n_triangles; i++) {
    if(level->textures[level->triangles[i]->material] == 0) {
      continue;
    }
    if(level->textures[level->triangles[i]->material] != tex) {
      if(tex != -1) {
        glEnd();
      }
      tex = level->textures[level->triangles[i]->material];
      glBindTexture(GL_TEXTURE_2D, tex);
      glBegin(GL_TRIANGLES);
    }

    vec_t shade = util_vec3_dot(level->triangles[i]->normal, level->sun_direction) * 0.6 + 0.4;

    if(level->materials[level->triangles[i]->material].fullbright) {
      glColor3f(1, 1, 1);
    } else {
      vec3_t color = {0, 0, 0};
      UTIL_VEC3_MA(color, level->ambient_color, 1.0 - shade);
      UTIL_VEC3_MA(color, level->sun_color, shade);

      glColor3f(color[0], color[1], color[2]);
    }

    for(int j = 0; j < 3; j++) {
      glTexCoord2f(level->triangles[i]->vertices[j].uv[0],
                   level->triangles[i]->vertices[j].uv[1]);

      glVertex3f(level->triangles[i]->vertices[j].pos[0],
                 level->triangles[i]->vertices[j].pos[1],
                 level->triangles[i]->vertices[j].pos[2]);
    }

  }
  glEnd();

  for(int i = 0; i < level->n_entities; i++) {
    entity_render(level->entities[i]);
  }
}

void level_render_sky(level_t *level) {
  glColor3f(1, 1, 1);
  glBindTexture(GL_TEXTURE_2D, level->sky_texture);
  glBegin(GL_QUAD_STRIP);
  int res = 6000;
  for(vec_t i = 0; i <= res; i++) {
    vec_t ang = (i / (float) res) * M_PI * 2;
    glTexCoord2f((ang / M_PI), 0.0);
    glVertex3f(-sin(ang) * 20.0, cos(ang) * 20.0, -20.0);
    glTexCoord2f((ang / M_PI), 1.0);
    glVertex3f(-sin(ang) * 20.0, cos(ang) * 20.0, 20.0);
  }
  glEnd();
}

/* get floor or ceiling height at position */

bool level_get_floor_ceiling(level_t *level, bool ceiling, vec3_t pos, vec_t *out_height, level_triangle_t **out_triangle) {
  vec_t closest_height = 0;
  vec_t closest_absheight = 0;

  level_triangle_t *closest_tri = NULL;

  for(int i = 0; i < level->n_triangles; i++) {
    level_triangle_t *triangle = level->triangles[i];

    int type = TRIANGLE_TYPE(triangle);

    if(type == (ceiling ? LEVEL_TRIANGLE_TYPE_CEILING : LEVEL_TRIANGLE_TYPE_FLOOR)) {
      bool is_in = is_in_triangle(pos, triangle, 0, 1);

      if(is_in) {
        vec_t height = triangle_height(triangle, pos);
        if(ceiling && height < pos[2]) {
          continue;
        }
        if(!ceiling && height > pos[2]) {
          continue;
        }
        vec_t dist = fabs(height - pos[2]);

        if(dist < closest_absheight || closest_tri == NULL) {
          closest_absheight = dist;
          closest_height = height;
          closest_tri = triangle;
        }
      }
    }
  }

  *out_height = closest_height;
  *out_triangle = closest_tri;

  if(closest_tri != NULL) {
    return true;
  }

  return false;
}

void level_collide_walls(level_t *level, entity_t *ent, vec3_t pos, vec_t radius, float z_offset, void (*oncollide)(struct entity_s *self, level_triangle_t *other, vec3_t normal)) {
  for(int i = 0; i < level->n_triangles; i++) {
    level_triangle_t *triangle = level->triangles[i];

    if(TRIANGLE_IS_WALL(triangle)) {
      vec_t d = util_vec3_dot(pos, triangle->normal);

      d -= triangle->distance;

      if(d >= -radius - 0.001 && d <= radius + 0.001) {
        vec3_t on_wall;

        UTIL_VEC3_COPY(on_wall, pos);
        UTIL_VEC3_MA(on_wall, triangle->normal, -d);

        on_wall[2] += z_offset;

        bool is_in = false;

        if(triangle->normal[0] < 0.5 && triangle->normal[0] > -0.5) {
          is_in = is_in_triangle(on_wall, triangle, 2, 0);
        } else {
          is_in = is_in_triangle(on_wall, triangle, 2, 1);
        }

        if(is_in) {
          if(triangle->doublesided && d < 0) {
            UTIL_VEC3_MA(pos, triangle->normal, (-radius) - d);
          } else {
            UTIL_VEC3_MA(pos, triangle->normal, radius - d);
          }
          if(oncollide != NULL) {
            oncollide(ent, triangle, triangle->normal);
          }
        }
      }
    }
  }
}

level_triangle_t *level_cast_ray(level_t *level, vec3_t start, vec3_t direction, vec3_t *out_position, vec_t *out_distance) {
  util_vec3_normalize(direction);
  double closest = 0.0;
  level_triangle_t *closest_tri = NULL;
  for(int i = 0; i < level->n_triangles; i++) {
    level_triangle_t *triangle = level->triangles[i];

    vec_t d1 = util_vec3_dot(start, triangle->normal) - triangle->distance;
    vec_t along = util_vec3_dot(direction, triangle->normal);
    if(d1 < 0 && triangle->doublesided)  {
      d1 *= -1;
      along *= -1;
    }
    if(d1 < 0 || along > 0 || along == 0) {
      continue;
    }
    vec3_t on_wall;

    UTIL_VEC3_COPY(on_wall, start);
    vec_t move = -d1 / along;
    UTIL_VEC3_MA(on_wall, direction, move);

    bool is_in;
    if(triangle->normal[0] < -0.6 || triangle->normal[0] > 0.6) {
      is_in = is_in_triangle(on_wall, triangle, 1, 2);
    } else if(triangle->normal[1] < -0.6 || triangle->normal[1] > 0.6) {
      is_in = is_in_triangle(on_wall, triangle, 0, 2);
    } else {
      is_in = is_in_triangle(on_wall, triangle, 0, 1);
    }
    if(is_in) {
      if(move < closest || closest_tri == NULL) {
        closest = move;
        closest_tri = triangle;
        UTIL_VEC3_COPY(*out_position, on_wall);
        *out_distance = closest;
      }
    }
  }
  return closest_tri;
}

static entity_t *_level_cast_ray_ent(level_t *level, vec3_t start, vec3_t direction, vec3_t *out_position, vec_t *out_distance, entity_t *ignore, bool ignore_z) {
  float z = direction[2];
  direction[2] = 0;
  z /= util_vec3_normalize(direction);
  vec3_t side = {direction[1], -direction[0], 0};
  double closest = 0.0;
  entity_t *closest_ent = NULL;
  for(int i = 0; i < level->n_entities; i++) {
    entity_t *ent = level->entities[i];
    if(ent == ignore) {
      continue;
    }
    if(ent->collision_type == ENT_COLLISION_TYPE_NONE  || ent->collision_type == ENT_COLLISION_TYPE_CORPSE || ent->collision_type == ENT_COLLISION_TYPE_TRIGGER) {
      continue;
    }

    vec_t d2 = util_vec3_dot(direction, ent->position);
    vec_t d3 = util_vec3_dot(side, ent->position);
    vec_t d4 = util_vec3_dot(direction, start);
    vec_t d5 = util_vec3_dot(side, start);
    d2 -= d4;
    d3 -= d5;
    if(d2 < 0) {
      continue;
    }
    if(d3 < -ent->radius || d3 > ent->radius) {
      continue;
    }
    if(d2 < closest || closest_ent == NULL) {
      vec3_t fwd;
      UTIL_VEC3_COPY(fwd, start);
      UTIL_VEC3_MA(fwd, direction, d2);
      fwd[2] += z * d2;
      if(!ignore_z) {
        if(fwd[2] < ent->position[2] || fwd[2] > ent->position[2] + ent->height) {
          continue;
        }
      }
      closest = d2;
      closest_ent = ent;
      UTIL_VEC3_COPY(*out_position, fwd);
      *out_distance = closest;
    }
  }
  return closest_ent;
}

entity_t *level_cast_ray_ent(level_t *level, vec3_t start, vec3_t direction, vec3_t *out_position, vec_t *out_distance, entity_t *ignore) {
  return _level_cast_ray_ent(level, start, direction, out_position, out_distance, ignore, false);
}

entity_t *level_cast_ray_ent_no_z(level_t *level, vec3_t start, vec3_t direction, vec3_t *out_position, vec_t *out_distance, entity_t *ignore) {
  return _level_cast_ray_ent(level, start, direction, out_position, out_distance, ignore, true);
}

int level_get_material(level_t *level, const char *name) {
  for(int i = 0; i < level->n_materials; i++) {
    if(!strcmp(level->materials[i].name, name)) {
      return i;
    }
  }
  level->n_materials++;
  level->textures = realloc(level->textures, sizeof(GLuint) * level->n_materials);
  level->materials = realloc(level->materials, sizeof(level_material_t) * level->n_materials);
  level_material_t mat;
  strcpy(mat.name, name);
  mat.fullbright = false;
  memcpy(&level->materials[level->n_materials - 1], &mat, sizeof(level_material_t));
  level->textures[level->n_materials - 1] = texture_load(name);
  return level->n_materials - 1;
}

level_triangle_t *level_add_triangle(level_t *level, level_triangle_t *tri) {
  level_triangle_t *copied = malloc(sizeof(level_triangle_t));
  memcpy(copied, tri, sizeof(level_triangle_t));
  level->n_triangles++;
  level->triangles = realloc(level->triangles, sizeof(level_triangle_t*) * level->n_triangles);
  level->triangles[level->n_triangles - 1] = copied;
  calc_values(level->triangles[level->n_triangles - 1]);
  return level->triangles[level->n_triangles - 1];
}
