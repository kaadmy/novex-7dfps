
#pragma once

#define DATA_PREFIX "data/"

#include "config.h"
#include "sprite.h"
#include "game.h"
#include "hud.h"
#include "spawn.h"
#include "sound.h"
#include "entity.h"
#include "level.h"
#include "texture.h"
#include "glad.h"
#include "main.h"
#include "renderer.h"
#include "types.h"
#include "util.h"
