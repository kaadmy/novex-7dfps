
#pragma once

#include "types.h"

#define UTIL_CLAMP(a, b, c) (a < b ? b : (a > c ? c : a))

// Vector 2 macros

#define UTIL_VEC2_EQUAL(a, b) (a[0] == b[0] && a[1] == b[1])
#define UTIL_VEC2_COPY(a, b) ((a)[0] = (b)[0], (a)[1] = (b)[1])
#define UTIL_VEC2_COPYCAST(a, b, t) ((a)[0] = ((t) (b)[0]), (a)[1] = ((t) (b)[1]))
#define UTIL_VEC2_SET(a, b) ((a)[0] = b, (a)[1] = (b))
#define UTIL_VEC2_ADD(a, b, c) ((a)[0] = (b)[0] + (c)[0], (a)[1] = (b)[1] + (c)[1])
#define UTIL_VEC2_SUB(a, b, c) ((a)[0] = (b)[0] - (c)[0], (a)[1] = (b)[1] - (c)[1])
#define UTIL_VEC2_MUL(a, b, c) ((a)[0] = (b)[0] * (c)[0], (a)[1] = (b)[1] * (c)[1])
#define UTIL_VEC2_DIV(a, b, c) ((a)[0] = (b)[0] / (c)[0], (a)[1] = (b)[1] / (c)[1])
#define UTIL_VEC2_MA(a, b, s) ((a)[0] += ((b)[0] * (s)), (a)[1] += ((b)[1] * (s)))

// Vector 3 macros

#define UTIL_VEC3_EQUAL(a, b) (a[0] == b[0] && a[1] == b[1] && a[2] == b[2])
#define UTIL_VEC3_COPY(a, b) ((a)[0] = (b)[0], (a)[1] = (b)[1], (a)[2] = (b)[2])
#define UTIL_VEC3_COPYCAST(a, b, t) ((a)[0] = ((t) (b)[0]), (a)[1] = ((t) (b)[1]), (a)[2] = ((t) (b)[2]))
#define UTIL_VEC3_SET(a, b) ((a)[0] = (b), (a)[1] = (b), (a)[2] = (b))
#define UTIL_VEC3_ADD(a, b, c) ((a)[0] = (b)[0] + (c)[0], (a)[1] = (b)[1] + (c)[1], (a)[2] = (b)[2] + (c)[2])
#define UTIL_VEC3_SUB(a, b, c) ((a)[0] = (b)[0] - (c)[0], (a)[1] = (b)[1] - (c)[1], (a)[2] = (b)[2] - (c)[2])
#define UTIL_VEC3_MUL(a, b, c) ((a)[0] = (b)[0] * (c)[0], (a)[1] = (b)[1] * (c)[1], (a)[2] = (b)[2] * (c)[2])
#define UTIL_VEC3_DIV(a, b, c) ((a)[0] = (b)[0] / (c)[0], (a)[1] = (b)[1] / (c)[1], (a)[2] = (b)[2] / (c)[2])
#define UTIL_VEC3_MA(a, b, s) ((a)[0] += ((b)[0] * (s)), (a)[1] += ((b)[1] * (s)), (a)[2] += ((b)[2] * (s)))

// Vector 4 macros

#define UTIL_VEC4_EQUAL(a, b) (a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3])
#define UTIL_VEC4_COPY(a, b) ((a)[0] = (b)[0], (a)[1] = (b)[1], (a)[2] = (b)[2], (a)[3] = (b)[3])
#define UTIL_VEC4_COPYCAST(a, b, t) ((a)[0] = ((t) (b)[0]), (a)[1] = ((t) (b)[1]), (a)[2] = ((t) (b)[2]), (a)[3] = ((t) (b)[3]))
#define UTIL_VEC4_SET(a, b) ((a)[0] = (b), (a)[1] = (b), (a)[2] = (b), (a)[3] = (b))
#define UTIL_VEC4_ADD(a, b, c) ((a)[0] = (b)[0] + (c)[0], (a)[1] = (b)[1] + (c)[1], (a)[2] = (b)[2] + (c)[2], (a)[3] = (b)[3] + (c)[3])
#define UTIL_VEC4_SUB(a, b, c) ((a)[0] = (b)[0] - (c)[0], (a)[1] = (b)[1] - (c)[1], (a)[2] = (b)[2] - (c)[2], (a)[3] = (b)[3] - (c)[3])
#define UTIL_VEC4_MUL(a, b, c) ((a)[0] = (b)[0] * (c)[0], (a)[1] = (b)[1] * (c)[1], (a)[2] = (b)[2] * (c)[2], (a)[3] = (b)[3] * (c)[3])
#define UTIL_VEC4_DIV(a, b, c) ((a)[0] = (b)[0] / (c)[0], (a)[1] = (b)[1] / (c)[1], (a)[2] = (b)[2] / (c)[2], (a)[3] = (b)[3] / (c)[3])
#define UTIL_VEC4_MA(a, b, s) ((a)[0] += ((b)[0] * (s)), (a)[1] += ((b)[1] * (s)), (a)[2] += ((b)[2] * (s)), (a)[3] += ((b)[3] * (s)))

// Vector 3 functions

vec_t util_vec3_dot(const vec3_t a, const vec3_t b);
vec_t util_vec3_length(const vec3_t a);
vec_t util_vec3_distance(const vec3_t a, const vec3_t b);
vec_t util_vec3_normalize(vec3_t a);
void util_vec3_clamp_aabb(vec3_t dest, const aabb_t aabb, const vec3_t src);
vec_t util_vec3_nearest_distance_aabb(const aabb_t aabb, const vec3_t position);
