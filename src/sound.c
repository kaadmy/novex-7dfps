
#include "local.h"

static ALCdevice *audio_device;
static ALCcontext *audio_context;

#define NUM_SOURCES 32
static ALuint sources[NUM_SOURCES];

#define BUFFER_SIZE 65536

void sound_init(void) {
  audio_device = alcOpenDevice(NULL);

  audio_context = alcCreateContext(audio_device, NULL);
  alcMakeContextCurrent(audio_context);
  for(int i = 0; i < NUM_SOURCES; i++) {
    alGenSources(1, &sources[i]);
  }
}

void sound_update(void) {
  double s = sin(game_state.camera_ent->angle);
  double c = cos(game_state.camera_ent->angle);
  
  ALfloat orientation[6] = {-s, c, 0, 0, 0, 1};
  alListener3f(AL_POSITION, game_state.camera_ent->position[0], game_state.camera_ent->position[1], game_state.camera_ent->position[2]);
  alListener3f(AL_VELOCITY, 0.0, 0.0, 0.0);
  alListenerfv(AL_ORIENTATION, orientation);
}

char *sound_load_data(const char *path, ALsizei *size, int *channels, int *rate) {
  char path2[128] = DATA_PREFIX;
  strcat(path2, path);
  FILE *f = fopen(path2, "rb");
  printf("Loaded file: %s\n", path2);
  
  OggVorbis_File oggfile;
  ov_open(f, &oggfile, NULL, 0);
  
  vorbis_info *info = ov_info(&oggfile, -1);
  *channels = info->channels;
  *rate = info->rate;

  int bit_stream = 0;
  long alloc_buffer = 0;
  char *buffer = malloc(BUFFER_SIZE);
  char *small_buffer = malloc(BUFFER_SIZE);
  long bytes;
  do {
    bytes = ov_read(&oggfile, small_buffer, BUFFER_SIZE, 0, 2, 1, &bit_stream);
    buffer = realloc(buffer, alloc_buffer + bytes);
    memcpy(buffer + alloc_buffer, small_buffer, bytes);
    alloc_buffer += bytes;
  } while(bytes > 0);
  free(small_buffer);
  ov_clear(&oggfile);
  *size = alloc_buffer;
  return buffer;
}

ALuint sound_load(const char *path) {
  ALuint buffer;
  alGenBuffers((ALuint) 1, &buffer);

  ALsizei size;
  char *data;

  int channels, rate;
  data = sound_load_data(path, &size, &channels, &rate);

  alBufferData(buffer, channels == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16, data, (ALsizei) size, rate);
  free(data);

  return buffer;
}

ALuint sound_play(ALuint sound, float x, float y, float z) {
  int source_idx = 0;
  for(int i = 0; i < NUM_SOURCES; i++) {
    int state;
    alGetSourcei(sources[i], AL_SOURCE_STATE, &state);
    if(state != AL_PLAYING) {
      source_idx = i;
    }
  }
  alSourcei(sources[source_idx], AL_SOURCE_RELATIVE, AL_FALSE);
  alSourcei(sources[source_idx], AL_BUFFER, sound);
  alSourcef(sources[source_idx], AL_PITCH, 1);
  alSourcef(sources[source_idx], AL_GAIN, 1);
  alSource3f(sources[source_idx], AL_POSITION, x, y, z);
  alSource3f(sources[source_idx], AL_VELOCITY, 0, 0, 0);
  alSourcei(sources[source_idx], AL_LOOPING, AL_FALSE);
  alSourcei(sources[source_idx], AL_REFERENCE_DISTANCE, 1.0);
  alSourcei(sources[source_idx], AL_MAX_DISTANCE, 1000000000.0);
  alSourcei(sources[source_idx], AL_MIN_GAIN, 0.0);
  alSourcei(sources[source_idx], AL_MAX_GAIN, 1.0);
  alSourcePlay(sources[source_idx]);
  return sources[source_idx];
}

ALuint sound_play_global(ALuint sound) {
  ALuint src = sound_play(sound, 0, 0, 0);
  alSourcei(src, AL_SOURCE_RELATIVE, AL_TRUE);
  alSourcef(src, AL_GAIN, 1.0);
  return src;
}
