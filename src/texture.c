
#include "local.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

typedef struct texture_s {
  char path[128];
  int w;
  int h;
  GLuint tex;
  struct texture_s *next;
} texture_t;

static bool texture_inited = false;
static texture_t *texture_cache_buckets[128];

static unsigned int hash(const char *path) {
  unsigned int o = 0;
  for(int i = 0; path[i] != '\0'; i++) {
    o <<= 1;
    o ^= (unsigned char) path[i];
  }
  return o;
}

GLuint texture_load(const char *path) {
  int w, h;
  return texture_load_with_size(path, &w, &h, false);
}

GLuint texture_load_with_size(const char *path, int *out_width, int *out_height, bool clamp) {
  if(!texture_inited) {
    printf("Initializing texture subsystem\n");
    texture_inited = true;
    for(int i = 0; i < 128; i++) {
      texture_cache_buckets[i] = NULL;
    }
  }
  unsigned int h = hash(path) & 127;
  texture_t *bucket = texture_cache_buckets[h];
  while(bucket != NULL) {
    if(!strcmp(path, bucket->path)) {
      *out_width = bucket->w;
      *out_height = bucket->h;
      return bucket->tex;
    }
    bucket = bucket->next;
  }

  char path2[128] = DATA_PREFIX;
  strcat(path2, path);
  stbi_set_flip_vertically_on_load(true);
  int size[2];
  int components;
  unsigned char *data = stbi_load(path2, &size[0], &size[1], &components, 4);

  printf("Loading image: %s\n", path2);

  if(data == NULL) {
    printf("Failed to load image\n");
  }

  GLuint tex;
  glGenTextures(1, &tex);

  glBindTexture(GL_TEXTURE_2D, tex);

  if(clamp) {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  } else {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  }

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size[0], size[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
  //glGenerateMipmap(GL_TEXTURE_2D);

  *out_width = size[0];
  *out_height = size[1];

  free(data);

  if(bucket == NULL) {
    texture_t *bucket = texture_cache_buckets[h];
    texture_t *prev = NULL;
    texture_t *new = malloc(sizeof(texture_t));
    new->w = size[0];
    new->h = size[1];
    new->tex = tex;
    new->next = NULL;
    strcpy(new->path, path);
    while(bucket != NULL) {
      prev = bucket;
      bucket = bucket->next;
    }
    if(prev == NULL) {
      texture_cache_buckets[h] = new;
    } else {
      prev->next = new;
    }
  }

  return tex;
}
