
#pragma once

entity_t *entity_init(level_t *level, const char *classname, vec3_t pos, float angle, bool startbackwards, bool remote, vec4_t customvars);
void entity_free(entity_t *entity);
void entity_update(entity_t *entity);
void entity_collide(entity_t *a, entity_t *b);
void entity_render(entity_t *entity);
void entity_render_view(entity_t *entity);
