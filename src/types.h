
#pragma once

#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include "glad.h"

#include <AL/al.h>
#include <AL/alc.h>

#include "vorbis/codec.h"
#include "vorbis/vorbisfile.h"

#include "config.h"

// Primitive types

#define ENT_SOUNDS_MAX 16

typedef float vec_t;
typedef vec_t vec2_t[2];
typedef vec_t vec3_t[3];
typedef vec_t vec4_t[4];

typedef int veci_t;
typedef veci_t vec2i_t[2];
typedef veci_t vec3i_t[3];
typedef veci_t vec4i_t[4];

typedef vec_t aabb_t[6]; // {min x, min y, min z, max x, max y, max z}

enum {
      INTERMISSION_TYPE_NONE = 0,
      INTERMISSION_TYPE_NORMAL,
      INTERMISSION_TYPE_ENDGAME
};

// Collision types
enum {
      ENT_COLLISION_TYPE_NONE, // never collides
      ENT_COLLISION_TYPE_CORPSE, // only collides with walls
      ENT_COLLISION_TYPE_STATIC, // never moves
      ENT_COLLISION_TYPE_DYNAMIC, // can move
      ENT_COLLISION_TYPE_TRIGGER, // does something on collision, doesn't move other things away
};

enum {
      CONTROL_LEFT = 1,
      CONTROL_RIGHT = 2,
      CONTROL_FORWARD = 4,
      CONTROL_BACK = 8,
      CONTROL_USE = 16,
      CONTROL_SHOOT = 32
};

// Level

enum {
      LEVEL_TRIANGLE_TYPE_FLOOR = 0,
      LEVEL_TRIANGLE_TYPE_WALL,
      LEVEL_TRIANGLE_TYPE_CEILING
};

enum {
      TEAM_NEUTRAL = 0,
      TEAM_PLAYER,
      TEAM_ENEMY,
      TEAM_ROGUE,
};

enum {
      WEAPON_SHOTGUN = 0,
      WEAPON_SUPERSHOTGUN,
      WEAPON_MINIGUN,
      WEAPON_ROCKETLAUNCHER,
      WEAPON_MAX
};

enum {
      INVENTORY_SHELLS = 0,
      INVENTORY_BULLETS,
      INVENTORY_ROCKETS,

      INVENTORY_SHOTGUN,
      INVENTORY_SUPERSHOTGUN,
      INVENTORY_MINIGUN,
      INVENTORY_ROCKETLAUNCHER,

      INVENTORY_HEALTH,
      INVENTORY_ARMOR,

      INVENTORY_KEY_RED,
      INVENTORY_KEY_YELLOW,
      INVENTORY_KEY_BLUE,

      INVENTORY_COLLECTIBLE,

      INVENTORY_MAX
};

struct entity_s;

typedef struct {
  vec3_t startpos;
  vec3_t pos;
  vec2_t uv;
} level_vertex_t;

typedef struct {
  // set to true when the player uses this triangle, not reset
  bool used;
  level_vertex_t vertices[3];
  vec3_t normal;
  vec_t distance; // distance from the origin, along normal
  vec3_t velocity;
  int material;
  bool doublesided;
  int entity;
  struct entity_s *entity_handle;
  struct entity_s *activator;
} level_triangle_t;

typedef struct {
  GLuint texture;
  int width;
  int height;
  int hframes;
  int vframes;
  int frame;
  vec_t rotation;

  vec_t animation_time;

  vec2_t offset;
} sprite_t;

typedef struct entity_s {
  int inventory[INVENTORY_MAX];
  int lock;

  bool is_player;

  bool nophys;
  bool noclip; // CHEATS
  bool remove; // set this when you want to remove this ent
  bool spawn_gibs; // if this is true, stuff can spawn gibs on hit
  vec4_t tint, viewtint;

  bool startbackwards;
  bool remote;

  int previous_weapon; // so it can tell when it changes
  int weapon;
  char classname[128];
  bool enabled;
  bool floating;

  int collision_type;
  vec_t mass;

  vec3_t position;
  vec3_t velocity;

  vec_t radius;
  vec_t height;
  float view_height;

  vec_t step_height;

  vec_t angle;
  vec_t angle_y;
  vec_t bounce;

  vec_t gravity;

  sprite_t sprite;
  sprite_t viewsprite;
  sprite_t viewsprites[WEAPON_MAX];

  vec_t bob_amount;

  // smoothed Z for going up steps
  float smoothed_z;
  float fall_bob;

  // movement options
  vec_t speed;
  vec_t friction;

  // bitfield:
  //  bits 0, 1, 2, 3, 4: left, right, forward, back, use respectively
  //  mouselook is controlled by modifying angle directly
  unsigned int controls;

  int n_triangles;
  level_triangle_t **triangles;

  vec_t customvars[4];

  vec_t state[16];

  float z_offset;

  int team;

  int painchance;

  ALuint sounds[ENT_SOUNDS_MAX];

  // entity handles
  struct entity_s *owner;
  struct entity_s *target;
  struct entity_s *activator;

  // callbacks
  void (*onspawn)(struct entity_s *self);
  void (*oncollideent)(struct entity_s *self, struct entity_s *other);
  void (*oncollidetri)(struct entity_s *self, level_triangle_t *other, vec3_t normal);
  void (*onupdate)(struct entity_s *self);
  void (*ondamage)(struct entity_s *self, struct entity_s *other, float damage);
  void (*onpickup)(struct entity_s *self, int item, int amount);
  void (*ontrigger)(struct entity_s *self, struct entity_s *activator);
  void (*oninput)(struct entity_s *self, int controls, vec2_t aim);
  bool (*onsquish)(struct entity_s *self, struct entity_s *other);
} entity_t;

typedef struct {
  bool fullbright;
  char name[128];
} level_material_t;

typedef struct {
  int n_triangles;
  level_triangle_t **triangles;

  int n_materials;
  GLuint *textures;
  level_material_t *materials;

  vec3_t ambient_color;
  vec3_t sun_color;
  vec3_t sun_direction;

  entity_t **entities;
  int n_entities;
  int alloc_entities;
  entity_t *player;

  GLuint sky_texture;
} level_t;

// HUD state

#define HUD_LOG_LENGTH 4
typedef struct {
  struct {
    char str[128];
    vec_t time;
    vec4_t color;
  } log[HUD_LOG_LENGTH];
  sprite_t font;
  struct {
    sprite_t shotgun;
    sprite_t minigun;
    sprite_t rocketlauncher;
  } weapons;
  struct {
    sprite_t health;
    sprite_t armor;
    sprite_t shells;
    sprite_t bullets;
    sprite_t rockets;

    sprite_t keys;

    sprite_t logo;
    sprite_t bg;
  } icons;

  struct {
    char from[128];
    char to[128];

    int kills, kills_total, items, items_total, collectibles, collectibles_total;
    vec_t time;

    vec_t start_time;

    bool skip; // skip counters
    bool finished; // start next level
  } intermission;

  struct {
    bool credits_shown;
    int selected_item;
  } menu;
} hud_state_t;

// Game state

typedef struct {
  double time_scale;
  double time_delta;
  double time_elapsed;

  struct {
    int kills, kills_total, items, items_total, collectibles, collectibles_total;
    double time;
  } level_stats;

  level_t *level;

  bool input_captured;
  entity_t *camera_ent;

  // which cheat was last used
  //  0: OFCLIP
  //  1: OFLVL##
  int cheat;
  int cheatnum;

  bool jodmode;

  vec4_t hud_viewport; // x, y, w, h

  bool use_debounce;

  struct {
    ALuint shotgun_fire;
    ALuint minigun_fire;
    ALuint rocketlauncher_fire;
    ALuint explosion;
    ALuint door_start;
    ALuint door_move;

    ALuint grunt_idle;
    ALuint grunt_wake;
    ALuint grunt_fire;

    ALuint fireball;

    ALuint pickup_item;
    ALuint pickup_armor;
    ALuint pickup_health;
    ALuint pickup_weapon;
    ALuint pickup_collectible;

    ALuint teleport;

    ALuint switch_activate, switch_return, switch_locked;
  } sounds;

  int sensitivity;

  int current_level;

  int warp_to_level;
  int intermission_type;

  bool in_game;

  int state_vars[32];

  sprite_t aimtarget;
} game_state_t;

// Global state

typedef struct {
  GLFWwindow *window;

  int window_size[2];

  double time_delta, time_delta_real;
  double time_elapsed;

  double mouse_delta[2];

  bool show_fps;

  bool menu_shown;
} global_state_t;
