
#pragma once

#include "types.h"

void sprite_load(const char *path, sprite_t *out_sprite);

void sprite_render(sprite_t *sprite, float anchor_x, float anchor_y, float scale, vec4_t color);
