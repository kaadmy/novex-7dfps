
#include "local.h"

hud_state_t hud_state;

void hud_init(void) {
  sprite_load("sprites/font.png", &hud_state.font);
  hud_state.font.hframes = 48;
  sprite_load("sprites/icon_health.png", &hud_state.icons.health);
  sprite_load("sprites/icon_armor.png", &hud_state.icons.armor);
  sprite_load("sprites/item_shells.png", &hud_state.icons.shells);
  sprite_load("sprites/item_bullets.png", &hud_state.icons.bullets);
  sprite_load("sprites/item_rockets.png", &hud_state.icons.rockets);
  sprite_load("sprites/icon_keys.png", &hud_state.icons.keys);
  hud_state.icons.keys.hframes = 3;

  sprite_load("sprites/item_shotgun.png", &hud_state.weapons.shotgun);
  sprite_load("sprites/item_minigun.png", &hud_state.weapons.minigun);
  sprite_load("sprites/item_rocketlauncher.png", &hud_state.weapons.rocketlauncher);

  sprite_load("sprites/logo.png", &hud_state.icons.logo);
  sprite_load("sprites/bg.png", &hud_state.icons.bg);

  for(int i = 0; i < HUD_LOG_LENGTH; i++) {
    hud_state.log[i].time = -1;
  }

  hud_state.menu.selected_item = 0;
  hud_state.menu.credits_shown = false;
}

void hud_render(entity_t *ent) {
  vec_t y = 0.0;
  for(int i = HUD_LOG_LENGTH - 1; i >= 0; i--) {
    if(hud_state.log[i].time > 0) {
      vec_t elapsed = game_state.time_elapsed - hud_state.log[i].time;
      if(elapsed > 4.3) continue;
      float o = 0;
      if(elapsed > 4) {
        y -= ((elapsed - 4) / 0.3) * 1.0;
        o -= ((elapsed - 4) / 0.3) * 1.0;
      }
      float flash_white = 0.0;
      if(elapsed < 0.3) {
        flash_white = 1.0 - (elapsed / 0.3);
      }
      vec4_t c;
      static const vec4_t white = {0, 0, 0, 1};
      UTIL_VEC4_SET(c, 0);
      c[3] = 1;
      UTIL_VEC4_MA(c, hud_state.log[i].color, (1.0 - (elapsed / 8.0)) * 1.0 - flash_white);
      UTIL_VEC4_MA(c, white, flash_white);
      hud_render_text(hud_state.log[i].str, 2, -y * 3.0 - 2 - o * 3, 0.0, 1.0, 0.5, c);
      y += 1.0;
    }
  }
  for(int i = 0, x = 0; i < 3; i++) {
    if(game_state.level->player->inventory[INVENTORY_KEY_RED + i] > 0) {
      hud_state.icons.keys.frame = i;
      hud_render_sprite_shadow(&hud_state.icons.keys, -2 - x, 9, 1.00, 0, 1.0);
      x += 6;
    }
  }

  hud_render_sprite_shadow(&hud_state.icons.health, 2, 10, 0, 0, 1.0);
  hud_render_sprite_shadow(&hud_state.icons.armor, 2, 2, 0, 0, 1.0);
  vec4_t red = {1, 0, 0, 1};
  vec4_t white = {1, 1, 1, 1};
  hud_render_number(ent->inventory[INVENTORY_ARMOR], 8, 1, 0, 0, 1.0, white);
  if(ent->inventory[INVENTORY_HEALTH] <= 30) {
    hud_render_number(ent->inventory[INVENTORY_HEALTH], 8, 9, 0, 0, 1.0, red);
  } else {
    hud_render_number(ent->inventory[INVENTORY_HEALTH], 8, 9, 0, 0, 1.0, white);
  }

  sprite_t *ammo_icon;
  int ammo_count;

  if(ent->weapon == WEAPON_SHOTGUN || ent->weapon == WEAPON_SUPERSHOTGUN) {
    ammo_icon = &hud_state.icons.shells;
    ammo_count = ent->inventory[INVENTORY_SHELLS];
  } else if(ent->weapon == WEAPON_MINIGUN) {
    ammo_icon = &hud_state.icons.bullets;
    ammo_count = ent->inventory[INVENTORY_BULLETS];
  } else {
    ammo_icon = &hud_state.icons.rockets;
    ammo_count = ent->inventory[INVENTORY_ROCKETS];
  }
  hud_render_sprite_shadow(ammo_icon, -2, 2, 1.0, 0.0, 1.0);
  hud_render_number(ammo_count, -21, 1, 1.0, 0, 1.0, white);
}

static vec4_t menuitem_color = {1, 1, 1, 1};
static vec4_t menuitem_sel_color = {1, 0.1, 0.2, 1};
static void hud_render_menuitem(const char *txt, int n, int max) {
  float y = (n - (max - 1) * 0.5) * -4.0;
  bool sel = n == hud_state.menu.selected_item;
  if((n - 1) == hud_state.menu.selected_item && n == 3) {
    sel = true;
  }
  float sz = 0.5;
  if(n == 1 && game_state.level == NULL) {
    vec4_t c = {0.2, 0.2, 0.2, 1.0};
    hud_render_text(txt, 0.0, y, 0.5, 0.5, sz, c);
  } else {
    hud_render_text(txt, 0.0, y, 0.5, 0.5, sz, sel ? menuitem_sel_color : menuitem_color);
  }
  if(n == hud_state.menu.selected_item) {
    float s = (game_state.hud_viewport[2] - 1) * 0.5 + pow(sin(global_state.time_elapsed * 6.0), 4.0);
    hud_render_text(">", -s + 6, y, 0.5, 0.5, sz, menuitem_sel_color);
    hud_render_text("<", s - 6, y, 0.5, 0.5, sz, menuitem_sel_color);
  }
}

void hud_render_menu_bg(void) {
  hud_render_sprite(&hud_state.icons.bg, 0.0, 0.0, 0.5, 0.5, 1.0);
  hud_render_sprite(&hud_state.icons.logo, 0.0, 20.0, 0.5, 0.5, 1.0);
}

void hud_render_menu(void) {
  vec4_t c = {0.0, 0.1, 0.2, 0.8};
  hud_tint(c);

  if(hud_state.menu.credits_shown) {
    const char *credits[] = {
                             "!LINKS AND LICENSES IN CREDITS.TXT",
                             "",
                             "!CODE",
                             "LITERALLYVOID",
                             "KAADMY",
                             "",
                             "!ART",
                             "LITERALLYVOID",
                             "KAADMY",
                             "",
                             "!SOUND EFFECTS",
                             "KAADMY",
                             "Q009",
                             "FINS",
                             "JULIEN MATTHEY",
                             "",
                             "!TOOLS USED",
                             "BLENDER",
                             "ASEPRITE",
                             "SFXR"
    };
    int l = sizeof(credits) / sizeof(credits[0]);
    vec4_t c1 = {0.6, 0.6, 0.6, 1.0};
    vec4_t c2 = {1.0, 1.0, 1.0, 1.0};
    for(int i = 0; i < l; i++) {
      if(credits[i][0] == '!') {
        hud_render_text(credits[i] + 1, 0.0, (i - ((l - 1) * 0.5)) * -3.0, 0.5, 0.5, 0.5, c1);
      } else {
        hud_render_text(credits[i], 0.0, (i - ((l - 1) * 0.5)) * -3.0, 0.5, 0.5, 0.4, c2);
      }
    }

    return;
  }

  int items = 6;
  hud_render_menuitem("NEW GAME", 0, items);
  hud_render_menuitem("RESTART LEVEL", 1, items);
  hud_render_menuitem("SENSITIVITY", 2, items);
  hud_render_menuitem("[           ]", 3, items);
  float y = (3 - (items - 1) * 0.5) * -4.0;
  hud_render_text("|", (game_state.sensitivity - 10) * 1.5, y, 0.5, 0.5, 0.5, hud_state.menu.selected_item == 2 ? menuitem_sel_color : menuitem_color);
  hud_render_menuitem("CREDITS", 4, items);
  hud_render_menuitem("QUIT", 5, items);

  hud_render_sprite(&hud_state.icons.logo, 0.0, 20.0, 0.5, 0.5, 0.5);
}

void hud_menu_input_key(int key, int action) {
#define DO_SKIP (hud_state.menu.selected_item == 1 && !game_state.in_game) || hud_state.menu.selected_item == 3
  if(hud_state.menu.credits_shown && action == GLFW_PRESS) {
    hud_state.menu.credits_shown = false;
    return;
  }
  bool do_click = false;
  int direction = 1;
  int it = hud_state.menu.selected_item;
  if(key == GLFW_KEY_UP || key == GLFW_KEY_W) {
    hud_state.menu.selected_item--;
    if(DO_SKIP) {
      hud_state.menu.selected_item--;
    }
  } else if(key == GLFW_KEY_DOWN || key == GLFW_KEY_S) {
    hud_state.menu.selected_item++;
    if(DO_SKIP) {
      hud_state.menu.selected_item++;
    }
  } else if(key == GLFW_KEY_LEFT || key == GLFW_KEY_A) {
    do_click = true;
    direction = -1;
  } else if(key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) {
    do_click = true;
  }
  if(key == GLFW_KEY_ENTER || key == GLFW_KEY_SPACE || do_click) {
    if(action == GLFW_PRESS) {
      if(hud_state.menu.selected_item == 0) {
        game_state.current_level = 0;
        game_state.warp_to_level = 11;
        game_state.intermission_type = INTERMISSION_TYPE_NONE;
        global_state.menu_shown = false;
      } else if(hud_state.menu.selected_item == 1) {
        game_state.warp_to_level = game_state.current_level;
        game_state.intermission_type = INTERMISSION_TYPE_NONE;
        global_state.menu_shown = false;
      } else if(hud_state.menu.selected_item == 4) {
        hud_state.menu.credits_shown = true;
      } else if(hud_state.menu.selected_item == 5) {
        glfwSetWindowShouldClose(global_state.window, true);
      }
    }
    if(hud_state.menu.selected_item == 2) {
      if(direction == -1) {
        game_state.sensitivity--;
      } else {
        game_state.sensitivity++;
      }
      if(game_state.sensitivity < 0) game_state.sensitivity = 20;
      if(game_state.sensitivity > 20) game_state.sensitivity = 0;
      ///game_state.sensitivity = UTIL_CLAMP(game_state.sensitivity, 0, 20);
    } else {
      sound_play_global(game_state.sounds.switch_activate);
    }
  }
  hud_state.menu.selected_item = UTIL_CLAMP(hud_state.menu.selected_item, 0, 5);
  if(it != hud_state.menu.selected_item) {
    sound_play_global(game_state.sounds.switch_activate);
  }
}

void hud_menu_input_mousemove(double x, double y) {
  if(hud_state.menu.credits_shown) {
    return;
  }
  //float y = (n - (max - 1) * 0.5) * -8.0;

  int w, h;
  glfwGetFramebufferSize(global_state.window, &w, &h);
  float viewport_y = game_state.hud_viewport[1] + ((y / (float) h) * game_state.hud_viewport[3]);
  //float y = (n - (max - 1) * 0.5) * -8.0;
  int len = 6;
  int item = floor((viewport_y / 4.0) + (len * 0.5));
  int old = hud_state.menu.selected_item;
  if(item != 1 || game_state.in_game) {
    if(item == 3) item = 2;
    hud_state.menu.selected_item = item;
  }
  hud_state.menu.selected_item = UTIL_CLAMP(hud_state.menu.selected_item, 0, 5);
  if(old != hud_state.menu.selected_item) {
    sound_play_global(game_state.sounds.switch_activate);
  }
}

void hud_menu_input_mousebutton(int button, int action, double x, double y) {
  if(hud_state.menu.credits_shown) {
    if(action == GLFW_PRESS) {
      hud_state.menu.credits_shown = false;
    }
    return;
  }
  if(button != GLFW_MOUSE_BUTTON_LEFT) return;
  int w, h;
  glfwGetFramebufferSize(global_state.window, &w, &h);
  hud_menu_input_mousemove(x, y);
  if(hud_state.menu.selected_item == 2) {
    float viewport_x = game_state.hud_viewport[0] + ((x / (float) w) * game_state.hud_viewport[2]);
    game_state.sensitivity = UTIL_CLAMP((viewport_x / 1.5) + 10.75, 0, 20);
  } else {
    if(action == GLFW_PRESS) {
      hud_menu_input_key(GLFW_KEY_ENTER, GLFW_PRESS);
    }
  }
}

void hud_render_intermission(void) {
  vec_t time_elapsed = game_state.time_elapsed - hud_state.intermission.start_time;
  vec4_t bg = {0.0, 0.0, 0.0, 1.0};
  vec4_t red = {0.9, 0.12, 0.22, 1.0};
  vec4_t white = {1, 1, 1, 1};
  char str[128];
  hud_tint(bg);
  if(time_elapsed < 6.6) {
    if(time_elapsed > 0.1) {
      hud_render_text(hud_state.intermission.from, 2.0, -2.0, 0.5, 1.0, 1.0, white);
    }
    if(time_elapsed > 0.2) {
      hud_render_text("CLEARED", 2.0, -10.0, 0.5, 1.0, 1.0, red);
    }
    if(time_elapsed > 0.6) {
      hud_render_text("KILLS", -30.0, 6.0, 0.5, 0.5, 1.0, white);
      sprintf(str, "%d/%d", (int) (hud_state.intermission.kills * UTIL_CLAMP((time_elapsed - 0.6) / 0.6, 0, 1)),
              hud_state.intermission.kills_total);
      hud_render_text(str, 30.0, 6.0, 0.5, 0.5, 1.0, white);
    }
    if(time_elapsed > 1.6) {
      hud_render_text("ITEMS", -30.0, -4.0, 0.5, 0.5, 1.0, white);
      sprintf(str, "%d/%d", (int) (hud_state.intermission.items * UTIL_CLAMP((time_elapsed - 1.6) / 0.6, 0, 1)),
              hud_state.intermission.items_total);
      hud_render_text(str, 30.0, -4.0, 0.5, 0.5, 1.0, white);
    }
    if(time_elapsed > 2.6) {
      hud_render_text("SECRETS", -30.0, -14.0, 0.5, 0.5, 1.0, white);
      sprintf(str, "%d/%d", (int) (hud_state.intermission.collectibles * UTIL_CLAMP((time_elapsed - 2.6) / 0.6, 0, 1)),
              hud_state.intermission.collectibles_total);
      hud_render_text(str, 30.0, -14.0, 0.5, 0.5, 1.0, white);
    }
    if(time_elapsed > 3.6) {
      int seconds = (int) (hud_state.intermission.time * UTIL_CLAMP((time_elapsed - 3.6) / 0.6, 0, 1));
      hud_render_text("TIME", -30.0, -24.0, 0.5, 0.5, 1.0, white);
      sprintf(str, "%02d:%02d", seconds / 60, seconds % 60);
      hud_render_text(str, 30.0, -24.0, 0.5, 0.5, 1.0, white);
    }
  } else {
    if(!strcmp(hud_state.intermission.to, "ENDGAME")) {
      const char *txt[] = {
                           "THANKS FOR PLAYING!",
                           "NOVEX WAS MADE FOR 7DFPS BY:",
                           "!LITERALLYVOID",
                           "!KAADMY",
                           "AND USES ASSETS BY:",
                           "!Q009",
                           "!FINS",
                           "!JULIEN MATTHEY"
      };
      for(int i = 0; i < sizeof(txt) / sizeof(txt[0]); i++) {
        if(txt[i][0] == '!') {
          hud_render_text(txt[i] + 1, 2.0, -2.0 - i * 4, 0.5, 1.0, 0.5, white);
        } else {
          vec4_t c = {0.5, 0.5, 0.5, 1.0};
          hud_render_text(txt[i], 2.0, -2.0 - i * 4, 0.5, 1.0, 0.5, c);
        }
      }
      hud_render_sprite(&hud_state.icons.logo, 0.0, 0.0, 0.5, 0.0, 0.5);
    } else {
      hud_render_text("NOW ENTERING", 2.0, -2.0, 0.5, 1.0, 1.0, red);
      hud_render_text(hud_state.intermission.to, 2.0, -10.0, 0.5, 1.0, 1.0, white);
    }
  }
}

bool hud_intermission_done(void) {
  vec_t time_elapsed = game_state.time_elapsed - hud_state.intermission.start_time;
  if(!strcmp(hud_state.intermission.to, "ENDGAME")) {
    return false;
  } else {
    return time_elapsed > 8.0;
  }
}

void hud_start_intermission(const char *from, const char *to,
                            int kills, int kills_total,
                            int items, int items_total,
                            int collectibles, int collectibles_total,
                            int time) {
  hud_state.intermission.start_time = game_state.time_elapsed;
  strcpy(hud_state.intermission.from, from);
  strcpy(hud_state.intermission.to, to);
  hud_state.intermission.kills = kills;
  hud_state.intermission.kills_total = kills_total;
  hud_state.intermission.items = items;
  hud_state.intermission.items_total = items_total;
  hud_state.intermission.collectibles = collectibles;
  hud_state.intermission.collectibles_total = collectibles_total;
  hud_state.intermission.time = time;
}

void hud_log(const char *txt) {
  vec4_t white = {1, 1, 1, 1};
  hud_log_color(txt, white);
}

void hud_log_color(const char *txt, vec4_t color) {
  memmove(&hud_state.log[1], &hud_state.log[0], sizeof(hud_state.log[0]) * (HUD_LOG_LENGTH - 1));
  UTIL_VEC4_COPY(hud_state.log[0].color, color);
  strcpy(hud_state.log[0].str, txt);
  hud_state.log[0].time = game_state.time_elapsed;
}

void hud_render_text(const char *txt, float x, float y, float anchor_x, float anchor_y, float scale, vec4_t color) {
  int i = 0;
  int len = strlen(txt);
  while(txt[i] != '\0') {
    if(txt[i] >= 'A' && txt[i] <= 'Z') {
      hud_state.font.frame = txt[i] - 'A';
    } else if(txt[i] == ' ') {
      i++;
      continue;
    } else if(txt[i] >= '0' && txt[i] <= '9') {
      hud_state.font.frame = (txt[i] - '0') + 26;
    } else if(txt[i] == '!') {
      hud_state.font.frame = 36;
    } else if(txt[i] == ',') {
      hud_state.font.frame = 37;
    } else if(txt[i] == '.') {
      hud_state.font.frame = 38;
    } else if(txt[i] == '/') {
      hud_state.font.frame = 39;
    } else if(txt[i] == '-') {
      hud_state.font.frame = 40;
    } else if(txt[i] == '+') {
      hud_state.font.frame = 41;
    } else if(txt[i] == ':') {
      hud_state.font.frame = 42;
    } else if(txt[i] == '>') {
      hud_state.font.frame = 43;
    } else if(txt[i] == '<') {
      hud_state.font.frame = 44;
    } else if(txt[i] == '|') {
      hud_state.font.frame = 45;
    } else if(txt[i] == '[') {
      hud_state.font.frame = 46;
    } else if(txt[i] == ']') {
      hud_state.font.frame = 47;
    }
    hud_render_sprite_color_shadow(&hud_state.font, x + (i - ((len - 1) * anchor_x)) * 6.0 * scale, y, anchor_x, anchor_y, scale, color);
    i++;
  }
}

void hud_render_number(int num, float x, float y, float anchor_x, float anchor_y, float scale, vec4_t color) {
  if(num < 0) num = 0;
  if(num > 999) num = 999;
  for(int i = 0; i < 3; i++) {
    hud_state.font.frame = ((num / (int) pow(10, 2 - i)) % 10) + 26;
    hud_render_sprite_color_shadow(&hud_state.font, x + i * 5, y, anchor_x, anchor_y, scale, color);
  }
}

void hud_render_sprite(sprite_t *sprite, float x, float y, float anchor_x, float anchor_y, float scale) {
  vec4_t color = {1.0, 1.0, 1.0, 1.0};
  hud_render_sprite_color(sprite, x, y, anchor_x, anchor_y, scale, color);
}

void hud_render_sprite_shadow(sprite_t *sprite, float x, float y, float anchor_x, float anchor_y, float scale) {
  vec4_t color = {1.0, 1.0, 1.0, 1.0};
  hud_render_sprite_color_shadow(sprite, x, y, anchor_x, anchor_y, scale, color);
}

void hud_render_sprite_color(sprite_t *sprite, float x, float y, float anchor_x, float anchor_y, float scale, vec4_t color) {
  glPushMatrix();
  glTranslatef(game_state.hud_viewport[0] + game_state.hud_viewport[2] * anchor_x + x, game_state.hud_viewport[1] + game_state.hud_viewport[3] * anchor_y + y, 0.0);
  sprite_render(sprite, anchor_x, anchor_y, scale, color);
  glPopMatrix();
}

void hud_render_sprite_color_shadow(sprite_t *sprite, float x, float y, float anchor_x, float anchor_y, float scale, vec4_t color) {
  vec4_t shadow = {0.0, 0.0, 0.0, 1.0};
  hud_render_sprite_color(sprite, x + 1 * scale, y - 1 * scale, anchor_x, anchor_y, scale, shadow);
  hud_render_sprite_color(sprite, x, y, anchor_x, anchor_y, scale, color);
}

void hud_tint(vec4_t c) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_ALPHA_TEST);
  glBindTexture(GL_TEXTURE_2D, 0);
  glColor4f(c[0], c[1], c[2], c[3]);
  glBegin(GL_QUADS);
  glVertex2f(game_state.hud_viewport[0],
             game_state.hud_viewport[1]);
  glVertex2f(game_state.hud_viewport[0] + game_state.hud_viewport[2],
             game_state.hud_viewport[1]);
  glVertex2f(game_state.hud_viewport[0] + game_state.hud_viewport[2],
             game_state.hud_viewport[1] + game_state.hud_viewport[3]);
  glVertex2f(game_state.hud_viewport[0],
             game_state.hud_viewport[1] + game_state.hud_viewport[3]);
  glEnd();
  glDisable(GL_BLEND);
  glEnable(GL_ALPHA_TEST);
}
