
#pragma once

#include "types.h"

void sound_init(void);
void sound_update(void);

ALuint sound_load(const char *path);
ALuint sound_play(ALuint sound, float x, float y, float z);
ALuint sound_play_global(ALuint sound);
