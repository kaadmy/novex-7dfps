
#pragma once

extern hud_state_t hud_state;

void hud_init(void);
void hud_render(entity_t *ent);
void hud_render_menu_bg(void);
void hud_render_menu(void);
void hud_menu_input_key(int key, int action);
void hud_menu_input_mousemove(double x, double y);
void hud_menu_input_mousebutton(int button, int action, double x, double y);
void hud_render_intermission(void); // returns true if intermission is done
bool hud_intermission_done(void);

void hud_start_intermission(const char *from, const char *to,
                            int kills, int kills_total,
                            int items, int items_total,
                            int collectibles, int collectibles_total,
                            int time);

void hud_log(const char *string);
void hud_log_color(const char *string, vec4_t color);
void hud_load_sprite(const char *path, sprite_t *out_sprite);
void hud_render_text(const char *txt, float x, float y, float anchor_x, float anchor_y, float scale, vec4_t color);
void hud_render_number(int num, float x, float y, float anchor_x, float anchor_y, float scale, vec4_t color);
void hud_render_sprite(sprite_t *sprite, float x, float y, float anchor_x, float anchor_y, float scale);
void hud_render_sprite_shadow(sprite_t *sprite, float x, float y, float anchor_x, float anchor_y, float scale);
void hud_render_sprite_color(sprite_t *sprite, float x, float y, float anchor_x, float anchor_y, float scale, vec4_t color);
void hud_render_sprite_color_shadow(sprite_t *sprite, float x, float y, float anchor_x, float anchor_y, float scale, vec4_t color);
void hud_tint(vec4_t c);
