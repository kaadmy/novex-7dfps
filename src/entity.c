
#include "local.h"
#include "hud.h"

entity_t *entity_init(level_t *level, const char *classname, vec3_t pos, float angle, bool startbackwards, bool remote, vec4_t customvars) {
  entity_t *entity = malloc(sizeof(entity_t));
  strcpy(entity->classname, classname);
  UTIL_VEC3_COPY(entity->position, pos);
  UTIL_VEC3_SET(entity->velocity, 0.0);
  entity->is_player = false;
  entity->noclip = false;
  entity->gravity = 1;
  entity->angle = angle;
  entity->angle_y = 0;
  entity->controls = 0;
  entity->sprite.texture = 0;
  entity->viewsprite.texture = 0;
  entity->viewsprite.animation_time = 0;
  entity->viewsprite.frame = 0;
  entity->smoothed_z = pos[2];
  entity->friction = 0;
  entity->mass = 1;
  entity->collision_type = ENT_COLLISION_TYPE_NONE;
  entity->step_height = 0.51;
  entity->floating = false;
  entity->enabled = false;
  entity->fall_bob = 0;
  entity->n_triangles = 0;
  entity->triangles = NULL;
  entity->startbackwards = startbackwards;
  entity->remote = remote;
  entity->z_offset = 0;
  entity->bounce = 0;
  entity->onspawn = NULL;
  entity->onupdate = NULL;
  entity->oncollidetri = NULL;
  entity->oncollideent = NULL;
  entity->ondamage = NULL;
  entity->onpickup = NULL;
  entity->oninput = NULL;
  entity->onsquish = NULL;
  entity->ontrigger = NULL;
  entity->owner = NULL;
  entity->activator = NULL;
  entity->target = NULL;
  entity->team = TEAM_NEUTRAL;
  entity->spawn_gibs = false;
  entity->remove = false;
  entity->view_height = 1.0;
  entity->radius = 1.0;
  entity->nophys = false;
  entity->height = 1.0;
  for(int i = 0; i < 16; i++) {
    entity->state[i] = 0.0;
    entity->sounds[i] = 0;
  }
  for(int i = 0; i < INVENTORY_MAX; i++) {
    entity->inventory[i] = 0;
  }
  UTIL_VEC4_SET(entity->tint, 1.0);
  UTIL_VEC4_SET(entity->viewtint, 0.0);
  UTIL_VEC4_COPY(entity->customvars, customvars);
  spawn_entity(entity);
  if(true) {
  } else if(!strcmp(classname, "rocket")) {
  } else if(!strcmp(classname, "sparks")) {
  } else if(!strcmp(classname, "explosion")) {
  } else if(!strcmp(classname, "func_switch")) {
    entity->floating = true;
    //sprite_load("tiles/switch01.png", &entity->sprite);
  } else if(!strcmp(classname, "func_door")) {
    //sprite_load("tiles/switch01.png", &entity->sprite);
  } else if(!strcmp(classname, "prop_barrel")) {
    entity->radius = 0.3;
    entity->height = 0.6;
    sprite_load("sprites/prop_barrel.png", &entity->sprite);
    entity->collision_type = ENT_COLLISION_TYPE_STATIC;
  } else if(!strcmp(classname, "prop_lamppost")) {
    entity->radius = 0.3;
    entity->height = 0.6;
    sprite_load("sprites/prop_lamppost.png", &entity->sprite);
    entity->collision_type = ENT_COLLISION_TYPE_STATIC;
  } else {
    printf("Unknown classname '%s'\n", classname);
  }
  return entity;
}

void entity_free(entity_t *entity) {
  free(entity->triangles);
  for(int i = 0; i < ENT_SOUNDS_MAX; i++) {
    if(entity->sounds[i] != 0) {
      alSourceStop(entity->sounds[i]);
      entity->sounds[i] = 0;
    }
  }
  free(entity);
}

void entity_update(entity_t *entity) {
  /*
  if(!strcmp(entity->classname, "func_switch")) {
    bool was_enabled = entity->enabled;
    for(int i = 0; i < entity->n_triangles; i++) {
      if(entity->triangles[i]->used) {
        entity->enabled = !entity->enabled;
        entity->triangles[i]->used = false;
      }
    }
    if((was_enabled && !entity->enabled) || (!was_enabled && entity->enabled)) {
      vec_t offset = was_enabled ? -0.5 : 0.5;
      for(int i = 0; i < entity->n_triangles; i++) {
        for(int j = 0; j < 3; j++) {
          entity->triangles[i]->vertices[j].uv[0] += offset;
        }
      }
    }
  } else if(!strcmp(entity->classname, "rocket")) {
  } else if(!strcmp(entity->classname, "explosion")) {
  } else if(!strcmp(entity->classname, "sparks")) {
  } else if(!strcmp(entity->classname, "func_door")) {
    }*/
  if(entity->onupdate != NULL) {
    entity->onupdate(entity);
  }
  if(entity->nophys) return;
  double spd = entity->speed * game_state.time_delta;
  double s = sin(entity->angle);
  double c = cos(entity->angle);
  vec3_t side = {-c, -s, 0};
  vec3_t forward = {-s, c, 0};

  if(entity->controls & CONTROL_LEFT) {
    UTIL_VEC3_MA(entity->velocity, side, spd);
  }
  if(entity->controls & CONTROL_RIGHT) {
    UTIL_VEC3_MA(entity->velocity, side, -spd);
  }
  if(entity->controls & CONTROL_FORWARD) {
    UTIL_VEC3_MA(entity->velocity, forward, spd);
  }
  if(entity->controls & CONTROL_BACK) {
    UTIL_VEC3_MA(entity->velocity, forward, -spd);
  }
  if(entity->controls & CONTROL_USE) {
    vec3_t start, dir = {0, 0, 0};
    UTIL_VEC3_COPY(start, entity->position);
    start[2] += 1;
    UTIL_VEC3_MA(dir, forward, 0.8);
    vec3_t end;
    vec_t dist;
    level_triangle_t *closest = level_cast_ray(game_state.level, start, dir, &end, &dist);
    if(closest != NULL && dist < 1.0) {
      closest->used = true;
      closest->activator = entity;
    }
  }

  UTIL_VEC3_MA(entity->position, entity->velocity, game_state.time_delta);
  vec3_t xy_vel;
  UTIL_VEC3_COPY(xy_vel, entity->velocity);
  xy_vel[2] = 0.0;
  UTIL_VEC3_MA(entity->velocity, xy_vel, -(1.0 - pow(entity->friction, game_state.time_delta)));

  vec_t height;
  level_triangle_t *tri;
  bool in_air = true;
  vec3_t pos;
  UTIL_VEC3_COPY(pos, entity->position);
  pos[2] += 0.6;
  float ceiling = -1;
  bool has_ceiling = false;
  entity_t *ceilent;
  if(level_get_floor_ceiling(game_state.level, true, pos, &height, &tri)) {
    if(entity->position[2] + entity->height > height) {
      if(entity->oncollidetri != NULL) {
        entity->oncollidetri(entity, tri, tri->normal);
      }
      entity->position[2] = height - entity->height;
      ceiling = height;
      has_ceiling = true;
      ceilent = tri->entity_handle;
      entity->velocity[2] = fabs(entity->velocity[2]) * -entity->bounce;
    }
  }
  if(level_get_floor_ceiling(game_state.level, false, pos, &height, &tri)) {
    if(entity->floating) {
      if(entity->position[2] < height) {
        entity->position[2] = height;
        entity->velocity[2] = fabs(entity->velocity[2]) * entity->bounce;
        if(entity->oncollidetri != NULL) {
          entity->oncollidetri(entity, tri, tri->normal);
        }
      }
    } else {
      if(entity->position[2] < height + entity->step_height + 0.1) {
        bool do_squish = true;
        if(has_ceiling) {
          if(ceilent != NULL && ceilent->onsquish != NULL) {
            do_squish = ceilent->onsquish(ceilent, entity);
          }
          if(entity->ondamage != NULL && do_squish) {
            entity->ondamage(entity, NULL, 99999);
          }
        }
        in_air = false;
        if(entity->oncollidetri != NULL) {
          entity->oncollidetri(entity, tri, tri->normal);
        }
        if(entity->velocity[2] < -0.5) {
          entity->fall_bob += (entity->velocity[2] + 0.5);
        }
        if(entity->bounce > 0.1) {
          entity->position[2] = height + entity->step_height + 0.1;
        } else {
          entity->position[2] = height;
        }
        entity->velocity[2] *= -entity->bounce;
      }
    }
  }
  if(!entity->floating) {
    if(in_air && !entity->noclip) {
      entity->velocity[2] -= game_state.time_delta * 20.0 * entity->gravity;
    }
  }

  if(entity->collision_type != ENT_COLLISION_TYPE_NONE && !entity->noclip) {
    level_collide_walls(game_state.level, entity, entity->position, entity->radius, entity->step_height, entity->oncollidetri);
    level_collide_walls(game_state.level, entity, entity->position, entity->radius, entity->height - 0.01, entity->oncollidetri);
  }
  entity->smoothed_z -= (entity->smoothed_z - entity->position[2]) * (1.0 - pow(0.0001, game_state.time_delta * 5.0));
  entity->fall_bob += game_state.time_delta * 1.0;
  entity->fall_bob = UTIL_CLAMP(entity->fall_bob, -0.6, 0.0);
  entity->smoothed_z = UTIL_CLAMP(entity->smoothed_z, entity->position[2] - 0.6, entity->position[2] + 0.6);
}

void entity_collide(entity_t *a, entity_t *b) {
  if(a->collision_type == ENT_COLLISION_TYPE_NONE) return;
  if(b->collision_type == ENT_COLLISION_TYPE_NONE) return;
  if(a->collision_type == ENT_COLLISION_TYPE_CORPSE) return;
  if(b->collision_type == ENT_COLLISION_TYPE_CORPSE) return;
  if(a->collision_type == ENT_COLLISION_TYPE_STATIC && b->collision_type == ENT_COLLISION_TYPE_STATIC) return;
  if(a->collision_type == ENT_COLLISION_TYPE_TRIGGER && b->collision_type == ENT_COLLISION_TYPE_TRIGGER) return;
  if(a->position[2] + a->height < b->position[2]) {
    return;
  }
  if(b->position[2] + b->height < a->position[2]) {
    return;
  }

  vec3_t offset;
  UTIL_VEC3_SUB(offset, a->position, b->position);
  offset[2] = 0;

  vec3_t offset_normalized;
  UTIL_VEC3_COPY(offset_normalized, offset);
  util_vec3_normalize(offset_normalized);

  vec_t d = util_vec3_length(offset);

  if(d > a->radius + b->radius) {
    return;
  }

  vec_t ratio = 0.5;

  if(a->collision_type == ENT_COLLISION_TYPE_STATIC) {
    ratio = 0;
  } else if(b->collision_type == ENT_COLLISION_TYPE_STATIC) {
    ratio = 1;
  } else {
    ratio = b->mass / (a->mass + b->mass);
  }

  if(a->collision_type == ENT_COLLISION_TYPE_TRIGGER) {
    if(a->oncollideent != NULL) {
      a->oncollideent(a, b);
    }
    return;
  } else if(b->collision_type == ENT_COLLISION_TYPE_TRIGGER) {
    if(b->oncollideent != NULL) {
      b->oncollideent(b, a);
    }
    return;
  }
  vec_t move = (a->radius + b->radius) - d;
  UTIL_VEC3_MA(a->position, offset_normalized, move * ratio);
  UTIL_VEC3_MA(b->position, offset_normalized, -move * (1 - ratio));
}

void entity_render(entity_t *entity) {
  glPushMatrix();
  glTranslatef(entity->position[0], entity->position[1], entity->position[2] - entity->z_offset / 16.0);
  glRotatef((game_state.camera_ent->angle * 180 / M_PI) + 180, 0, 0, 1);
  glRotatef(-90.0, 1.0, 0.0, 0.0);
  glRotatef(180.0, 0.0, 0.0, 1.0);
  sprite_render(&entity->sprite, 0.5, 0.0, 1 / 16.0, entity->tint);
  if(true) {
    glTranslatef(0.0, entity->height * 0.5, 0.1);
    if(game_state.level->player->target == entity) {
      vec4_t c = {1, 1, 1, 1};
      sprite_render(&game_state.aimtarget, 0.5, 0.5, (1 / 16.0) * (sin(game_state.time_elapsed * 15.0) * 0.1 + 1.0), c);
    }
  }
  glPopMatrix();
  /*
                                                       
  if(entity->sprite.texture != 0) {
    glBindTexture(GL_TEXTURE_2D, entity->sprite.texture);
    vec_t w = entity->sprite.width / 16.0 / entity->sprite.hframes;
    vec_t h = entity->sprite.height / 16.0 / entity->sprite.vframes;
    vec2_t start = {0, 0};
    vec2_t end = {1.0 / entity->sprite.hframes, 1.0 / entity->sprite.vframes};
    int xframe = entity->sprite.frame % entity->sprite.hframes;
    int yframe = entity->sprite.frame / entity->sprite.hframes;
    start[0] += end[0] * xframe;
    start[1] += end[1] * yframe;
    end[0] += end[0] * xframe;
    end[1] += end[1] * yframe;
    glPushMatrix();
    glTranslatef(entity->position[0], entity->position[1], entity->position[2] - entity->z_offset / 16.0);
    glRotatef((game_state.camera_ent->angle * 180 / M_PI) + 180.0, 0, 0, 1);
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
    glTexCoord2f(start[0], start[1]);
    glVertex3f(w * 0.5, 0, 0);
    glTexCoord2f(end[0], start[1]);
    glVertex3f(-w * 0.5, 0, 0);
    glTexCoord2f(end[0], end[1]);
    glVertex3f(-w * 0.5, 0, h);
    glTexCoord2f(start[0], end[1]);
    glVertex3f(w * 0.5, 0, h);
    glEnd();
    glPopMatrix();
    }*/
}

void entity_render_view(entity_t *entity) {
  double t = sin(game_state.time_elapsed * 4.0) * 1.5;
  double bob = entity->bob_amount;
  hud_render_sprite(&entity->viewsprite, sin(t) * 7 * bob, -fabs(cos(t) * 10) * bob, 0.5, 0, 1.0);
  hud_render(entity);
  hud_tint(entity->viewtint);
}
