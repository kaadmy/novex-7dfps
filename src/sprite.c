
#include "local.h"
#include "sprite.h"

void sprite_load(const char *path, sprite_t *out_sprite) {
  out_sprite->texture = texture_load_with_size(path, &out_sprite->width, &out_sprite->height, true);
  out_sprite->hframes = 1;
  out_sprite->vframes = 1;
  out_sprite->frame = 0;
  out_sprite->animation_time = 0;
  out_sprite->rotation = 0;
  out_sprite->offset[0] = 0;
  out_sprite->offset[1] = 0;
}

void sprite_render(sprite_t *sprite, float anchor_x, float anchor_y, float scale, vec4_t color) {
  if(sprite->texture != 0) {
    glBindTexture(GL_TEXTURE_2D, sprite->texture);
    vec_t w = (sprite->width / sprite->hframes) * scale;
    vec_t h = (sprite->height / sprite->vframes) * scale;
    vec2_t start = {0, 0};
    vec2_t end = {1.0 / sprite->hframes, 1.0 / sprite->vframes};
    int xframe = sprite->frame % sprite->hframes;
    int yframe = sprite->frame / sprite->hframes;
    start[0] += end[0] * xframe;
    start[1] += end[1] * yframe;
    end[0] += end[0] * xframe;
    end[1] += end[1] * yframe;
    glPushMatrix();
    glTranslatef(-w * (anchor_x - 0.5),
                 -h * (anchor_y - 0.5), 0);
    glRotatef(sprite->rotation, 0, 0, 1);
    glTranslatef(sprite->offset[0] * scale, sprite->offset[1] * scale, 0.0);
    glColor4fv(color);
    glBegin(GL_QUADS);
    glTexCoord2f(start[0], start[1]);
    glVertex2f(-w * 0.5, -h * 0.5);
    glTexCoord2f(end[0], start[1]);
    glVertex2f(w * 0.5, -h * 0.5);
    glTexCoord2f(end[0], end[1]);
    glVertex2f(w * 0.5, h * 0.5);
    glTexCoord2f(start[0], end[1]);
    glVertex2f(-w * 0.5, h * 0.5);
    glEnd();
    glPopMatrix();
  }
}
