
#include "local.h"

global_state_t global_state;

/* event callbacks */

static void event_resize(GLFWwindow *window, int width, int height) {
}
static void event_key(GLFWwindow *window, int key, int scancode, int action, int mod) {
  if(key == GLFW_KEY_F1 && action == GLFW_PRESS) global_state.show_fps = !global_state.show_fps;
  static int seq[16] = {0};
  if(action == GLFW_PRESS) {
    memmove(&seq[0], &seq[1], sizeof(int) * 15);
    seq[15] = key;
  }
  static char codes[] = {
                         'C', 'L', 'I', 'P', '\0',
                         'L', 'V', 'L', '#', '#', '\0',
                         'K', 'F', 'A', '\0',
                         'F', 'A', '\0',
                         'J', 'O', 'D', '\0',
                         'W', 'H', 'E', 'N', 'S', 'T', '\0',
                         '\0'
  };
  for(int k = 0; k < 15; k++) {
    if(seq[k] != GLFW_KEY_O || seq[k + 1] != GLFW_KEY_F) {
      continue;
    }
    int j = 0;
    int match = 0;
    int cheat_idx = 0;
    int num = 0;
    for(int i = 0; ; i++) {
      if((j + k + 2) > 15) {

      } else {
        if(seq[j + k + 2] == codes[i] || codes[i] == '#') {
          if(codes[i] == '#') {
            num *= 10;
            num += seq[j + k + 2] - '0';
          }
          match++;
        }
      }
      ++j;
      if(codes[i] == '\0') {
        if(match == j - 1) {
          if(cheat_idx == 5) {
            static const char *messages[] = {
                                             "WHENST YOU PLAY NOVEX",
                                             "WHENST YOU GET A HELMET",
                                             "WHENST YOU GET SOME HEALTH",
                                             "WHENST YOU GET SOME GUNS",
                                             "WHENST YOU GET A SHOTGUN",
                                             "WHENST YOU GET A SUPER SHOTGUN",
                                             "WHENST YOU GET A LAWN CHAIR",
                                             "WHENST YOU GET A ROPE",
                                             "WHENST YOU GET A SEGMENT FAULT",
                                             "WHENST YOU GET A NOLE POINTER",
                                             "WHENST GAME!!!",
                                             "WHENST CHEAT ENABLED",
                                             "WHENST QUOTE",
            };
            hud_log(messages[rand() % (sizeof(messages) / sizeof(messages[0]))]);
          }
          game_state.cheat = cheat_idx;
          game_state.cheatnum = num;
          for(int j = 0; j < 16; j++) {
            seq[j] = '\0';
          }
        }
        cheat_idx++;
        match = 0;
        j = 0;
        num = 0;
        if(codes[i + 1] == '\0') {
          break;
        }
      }
    }
  }
  if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    sound_play_global(game_state.sounds.door_start);
    global_state.menu_shown = !global_state.menu_shown;
  }
  if(global_state.menu_shown) {
    if(action == GLFW_PRESS || action == GLFW_REPEAT) {
      hud_menu_input_key(key, action);
    }
  }
}
static void event_mouse_button(GLFWwindow *window, int button, int action, int mod) {
  if(global_state.menu_shown) {
    double x, y;
    glfwGetCursorPos(global_state.window, &x, &y);
    hud_menu_input_mousebutton(button, action, x, y);
  }
}
static void event_mouse_move(GLFWwindow *window, double x, double y) {
  static double prev[2];

  if(prev[0] != 0) {
    global_state.mouse_delta[0] += x - prev[0];
    global_state.mouse_delta[1] += y - prev[1];
  }
  prev[0] = x;
  prev[1] = y;

  if(global_state.menu_shown) {
    hud_menu_input_mousemove(x, y);
  }
}

/* entry point */

int main(int argc, char **argv) {
  global_state.menu_shown = false;
  printf(CONFIG_GAME_TITLE " version " CONFIG_VERSION_STRING "\n");

  /* init glfw */

  if(!glfwInit()) {
    printf("Failed to initialize GLFW\n");

    return 1;
  }

  /* glfw hints */

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

  glfwSwapInterval(0);

  /* create and verify window */

  global_state.window = glfwCreateWindow(1024, 576, CONFIG_GAME_TITLE, NULL, NULL);

  if(global_state.window == NULL) {
    printf("Failed to create window\n");

    glfwTerminate();

    return false;
  }

  glfwMakeContextCurrent(global_state.window);

  /* init glad */

  if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
    printf("Failed to initialize GLAD\n");

    glfwDestroyWindow(global_state.window);
    glfwTerminate();

    return 1;
  }

  /* bind event callbacks */

  glfwSetWindowSizeCallback(global_state.window, event_resize);
  glfwSetKeyCallback(global_state.window, event_key);
  glfwSetMouseButtonCallback(global_state.window, event_mouse_button);
  glfwSetCursorPosCallback(global_state.window, event_mouse_move);

  sound_init();

  /* init renderer */

  renderer_init();

  /* init game */

  game_init();

  /* mainloop */

  glfwSetInputMode(global_state.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  //ALuint snd = sound_load("music/S31-Futuristic-Resources.ogg");
  //ALuint src = sound_play(snd, 0, 0, 0);
  //alSourcei(src, AL_LOOPING, AL_TRUE);
  //alSourcef(src, AL_GAIN, 0.3);

  global_state.time_elapsed = glfwGetTime();
  global_state.show_fps = false;

  double prev = glfwGetTime();
  
  while(!glfwWindowShouldClose(global_state.window)) {
    if(global_state.menu_shown) {
      glfwSetInputMode(global_state.window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    } else {
      glfwSetInputMode(global_state.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
    game_state.input_captured = !global_state.menu_shown;

    glfwPollEvents();
    if(glfwGetMouseButton(global_state.window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS && global_state.menu_shown) {
      double x, y;
      glfwGetCursorPos(global_state.window, &x, &y);
      hud_menu_input_mousebutton(GLFW_MOUSE_BUTTON_LEFT, GLFW_REPEAT, x, y);
    }

    double t = glfwGetTime();
    global_state.time_delta = t - prev;
    prev = t;
    global_state.time_delta_real = global_state.time_delta;
    if(global_state.time_delta > (1 / 60.0)) {
      global_state.time_delta = (1 / 60.0);
    }
    global_state.time_elapsed += global_state.time_delta;
    glfwGetFramebufferSize(global_state.window, &global_state.window_size[0], &global_state.window_size[1]);

    game_update();

    global_state.mouse_delta[0] = 0;
    global_state.mouse_delta[1] = 0;

    glfwSwapBuffers(global_state.window);
  }

  glfwDestroyWindow(global_state.window);
  glfwTerminate();

  return 0;
}
