
#include "local.h"

game_state_t game_state;

/* game init */

void game_init(void) {
  game_state.time_scale = 1.0;
  game_state.time_delta = 1.0 / 60.0; // dummy value
  game_state.time_elapsed = 0.0;
  game_state.cheat = -1;
  game_state.jodmode = false;
  game_state.level = NULL;
  game_state.sensitivity = 4;
  game_state.current_level = 0;
  game_state.warp_to_level = 0;

  //game_load_level(11);

  hud_init();

  game_state.sounds.shotgun_fire = sound_load("sounds/shotgun_fire.ogg");
  game_state.sounds.minigun_fire = sound_load("sounds/minigun_fire.ogg");
  game_state.sounds.rocketlauncher_fire = sound_load("sounds/rocketlauncher_fire.ogg");
  game_state.sounds.explosion = sound_load("sounds/explosion.ogg");

  game_state.sounds.door_start = sound_load("sounds/door_lock.ogg");
  game_state.sounds.door_move = sound_load("sounds/door_move.ogg");

  game_state.sounds.grunt_idle = sound_load("sounds/grunt_idle.ogg");
  game_state.sounds.grunt_wake = sound_load("sounds/grunt_wake.ogg");
  game_state.sounds.grunt_fire = sound_load("sounds/grunt_fire.ogg");

  game_state.sounds.pickup_item = sound_load("sounds/pickup_item.ogg");
  game_state.sounds.pickup_armor = sound_load("sounds/pickup_armor.ogg");
  game_state.sounds.pickup_health = sound_load("sounds/pickup_health.ogg");
  game_state.sounds.pickup_weapon = sound_load("sounds/pickup_weapon.ogg");
  game_state.sounds.pickup_collectible = sound_load("sounds/pickup_collectible.ogg");

  game_state.sounds.switch_activate = sound_load("sounds/switch_activate.ogg");
  game_state.sounds.switch_return = sound_load("sounds/switch_return.ogg");
  game_state.sounds.switch_locked = sound_load("sounds/switch_locked.ogg");

  sprite_load("sprites/aimtarget.png", &game_state.aimtarget);
}

static void reset_vars(void) {
  for(int i = 0; i < INVENTORY_MAX; i++) {
    game_state.state_vars[i] = 0;
  }
  game_state.state_vars[INVENTORY_HEALTH] = 100;
  game_state.state_vars[INVENTORY_ARMOR] = 0;
  game_state.state_vars[INVENTORY_SHELLS] = 10;
  game_state.state_vars[INVENTORY_SHOTGUN] = 1;
}

void game_unload_level(void) {
  if(game_state.level != NULL && game_state.intermission_type == INTERMISSION_TYPE_NORMAL) {
    entity_t *player = game_state.level->player;
    for(int i = 0; i < INVENTORY_MAX; i++) {
      if(i >= INVENTORY_KEY_RED && i <= INVENTORY_KEY_BLUE) {
        continue;
      }
      if(i == INVENTORY_HEALTH && player->inventory[i] < 50) {
        game_state.state_vars[i] = 50;
      } else {
        game_state.state_vars[i] = player->inventory[i];
      }
    }
  }
  level_free(game_state.level);
  game_state.level = NULL;
  game_state.camera_ent = NULL;
}

void game_load_level(int level) {
  game_state.in_game = true;
  game_state.level_stats.kills = 0;
  game_state.level_stats.kills_total = 0;
  game_state.level_stats.items = 0;
  game_state.level_stats.items_total = 0;
  game_state.level_stats.collectibles = 0;
  game_state.level_stats.collectibles_total = 0;
  game_state.level_stats.time = 0;
  game_unload_level();
  char lvl[] = "e1m1";

  lvl[1] = (level / 10) + '0';
  lvl[3] = (level % 10) + '0';

  char str[128] = "levels/";

  strcat(str, lvl);
  strcat(str, ".nlv");

  game_state.level = level_init(str);
  if(game_state.level != NULL) {
    if(game_state.current_level == 0) {
      reset_vars();
    }
    for(int i = 0; i < INVENTORY_MAX; i++) {
      game_state.level->player->inventory[i] = game_state.state_vars[i];
    }
    game_state.camera_ent = game_state.level->player;
    game_state.current_level = level;
  } else {
    game_state.current_level = 0;
  }
}

/* game update (called from mainloop) */

static const char *level_name(int l) {
  static const char *names[] = {
                                "ENTRYWAY",
                                "TREATMENT PLANT",
                                "ENDGAME"
  };
  return names[l - 11];
}

void game_update(void) {
  game_state.time_delta = global_state.time_delta * game_state.time_scale;

  if(global_state.menu_shown) {
    game_state.time_delta = 0.0;
  }

  game_state.time_elapsed += game_state.time_delta;

  game_state.level_stats.time += game_state.time_delta;
  if(game_state.warp_to_level > 0) {
    game_unload_level();
    if(game_state.intermission_type == INTERMISSION_TYPE_NORMAL) {
      hud_start_intermission(level_name(game_state.current_level),
                             level_name(game_state.warp_to_level),
                             game_state.level_stats.kills,
                             game_state.level_stats.kills_total,
                             game_state.level_stats.items,
                             game_state.level_stats.items_total,
                             game_state.level_stats.collectibles,
                             game_state.level_stats.collectibles_total,
                             game_state.level_stats.time);
    } else {
      game_load_level(game_state.warp_to_level);
    }
    game_state.current_level = game_state.warp_to_level;
    game_state.warp_to_level = 0;
  }
  if(game_state.level == NULL && game_state.current_level != 0) {
    if(hud_intermission_done()) {
      game_load_level(game_state.current_level);
    }
  }
  if(game_state.level != NULL) {
    sound_update();

    if(game_state.cheat == 0) {
      hud_log("NOCLIP TOGGLED");
      game_state.level->player->noclip = !game_state.level->player->noclip;
    } else if(game_state.cheat == 1) {
      game_load_level(game_state.cheatnum);
      char str[] = "WARP TO LEVEL E1M1";
      str[15] = (game_state.cheatnum / 10) + '0';
      str[17] = (game_state.cheatnum % 10) + '0';
      hud_log(str);
    } else if(game_state.cheat == 2) {
      hud_log("YOU GOT KEYS/WEAPONS/AMMO");
      for(int i = 0; i < INVENTORY_MAX; i++) {
        if(i != INVENTORY_COLLECTIBLE) {
          game_state.level->player->onpickup(game_state.level->player, i, 9999);
        }
      }
    } else if(game_state.cheat == 3) {
      hud_log("YOU GOT WEAPONS/AMMO");
      for(int i = 0; i < INVENTORY_MAX; i++) {
        if(i != INVENTORY_COLLECTIBLE && (i < INVENTORY_KEY_RED || i > INVENTORY_KEY_BLUE)) {
          game_state.level->player->onpickup(game_state.level->player, i, 9999);
        }
      }
    } else if(game_state.cheat == 4) {
      hud_log("JODMODE TOGGLED");
      game_state.jodmode = !game_state.jodmode;
    }

    game_state.cheat = -1;

    int controls = 0;
    vec2_t aim = {0, 0};

    if(game_state.input_captured) {
      if(glfwGetKey(global_state.window, GLFW_KEY_A) == GLFW_PRESS || glfwGetKey(global_state.window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        controls |= CONTROL_LEFT;
      }
      if(glfwGetKey(global_state.window, GLFW_KEY_D) == GLFW_PRESS || glfwGetKey(global_state.window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        controls |= CONTROL_RIGHT;
      }
      if(glfwGetKey(global_state.window, GLFW_KEY_W) == GLFW_PRESS || glfwGetKey(global_state.window, GLFW_KEY_UP) == GLFW_PRESS) {
        controls |= CONTROL_FORWARD;
      }
      if(glfwGetKey(global_state.window, GLFW_KEY_S) == GLFW_PRESS || glfwGetKey(global_state.window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        controls |= CONTROL_BACK;
      }
      if(glfwGetKey(global_state.window, GLFW_KEY_SPACE) == GLFW_PRESS) {
        if(!game_state.use_debounce) {
          game_state.use_debounce = true;
          controls |= CONTROL_USE;
        }
      } else {
        game_state.use_debounce = false;
      }
      if(glfwGetMouseButton(global_state.window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
        controls |= CONTROL_SHOOT;
      }
      if(glfwGetKey(global_state.window, GLFW_KEY_1) == GLFW_PRESS) {
        game_state.level->player->weapon = WEAPON_SHOTGUN;
      }
      if(glfwGetKey(global_state.window, GLFW_KEY_2) == GLFW_PRESS) {
        game_state.level->player->weapon = WEAPON_SUPERSHOTGUN;
      }
      if(glfwGetKey(global_state.window, GLFW_KEY_3) == GLFW_PRESS) {
        game_state.level->player->weapon = WEAPON_MINIGUN;
      }
      if(glfwGetKey(global_state.window, GLFW_KEY_4) == GLFW_PRESS) {
        game_state.level->player->weapon = WEAPON_ROCKETLAUNCHER;
      }

      aim[0] = -global_state.mouse_delta[0] * 0.003;
      aim[1] = -global_state.mouse_delta[1] * 0.003;
    }
    if(game_state.level->player->oninput != NULL) {
      game_state.level->player->oninput(game_state.level->player, controls, aim);
    }

    level_update(game_state.level);
  }

  renderer_render();
}

/* main rendering (called from renderer_render) */

// code from https://www.khronos.org/opengl/wiki/GluPerspective_code

/*void glhFrustumf2(float *matrix, float left, float right, float bottom, float top,
                  float znear, float zfar)
{
    float temp, temp2, temp3, temp4;
    temp = 2.0 * znear;
    temp2 = right - left;
    temp3 = top - bottom;
    temp4 = zfar - znear;
    matrix[0] = temp / temp2;
    matrix[1] = 0.0;
    matrix[2] = 0.0;
    matrix[3] = 0.0;
    matrix[4] = 0.0;
    matrix[5] = temp / temp3;
    matrix[6] = 0.0;
    matrix[7] = 0.0;
    matrix[8] = (right + left) / temp2;
    matrix[9] = (top + bottom) / temp3;
    matrix[10] = (-zfar - znear) / temp4;
    matrix[11] = -1.0;
    matrix[12] = 0.0;
    matrix[13] = 0.0;
    matrix[14] = (-temp * zfar) / temp4;
    matrix[15] = 0.0;
    }*/

void glhPerspectivef2(float fovyInDegrees, float aspectRatio,
                      float znear, float zfar) {

    float ymax, xmax;
    ymax = znear * tanf(fovyInDegrees * M_PI / 360.0);
    // ymin = -ymax;
    // xmin = -ymax * aspectRatio;
    xmax = ymax * aspectRatio;
    glFrustum(-xmax, xmax, -ymax, ymax, znear, zfar);
}

void game_render(void) {
  if(game_state.level != NULL) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glhPerspectivef2(90.0f, (float) global_state.window_size[0] / global_state.window_size[1], 0.05, 100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(-90.0, 1, 0, 0);
    glRotatef(-game_state.camera_ent->angle_y * 180 / M_PI, 1, 0, 0);
    glRotatef(-game_state.camera_ent->angle * 180 / M_PI, 0, 0, 1);

    level_render_sky(game_state.level);

    glClear(GL_DEPTH_BUFFER_BIT);

    glTranslatef(-game_state.camera_ent->position[0], -game_state.camera_ent->position[1], -(game_state.camera_ent->smoothed_z + game_state.camera_ent->fall_bob + game_state.camera_ent->view_height));

    glEnable(GL_FOG);
    GLfloat fogColor[4] = {0.3, 0.1, 0.0, 1.0};
    glFogfv(GL_FOG_COLOR, fogColor);
    glFogf(GL_FOG_DENSITY, 0.02);
    glFogi(GL_FOG_MODE, GL_EXP2);

    level_render(game_state.level);

    glDisable(GL_FOG);
  }

  glPushMatrix();
  glLoadIdentity();

  vec4_t viewport = {-64, -36, 128, 72};
  float fake_aspect = viewport[2] / viewport[3];
  float real_aspect = (float) global_state.window_size[0] / global_state.window_size[1];
  if(fake_aspect < real_aspect) {
    float mul = real_aspect / fake_aspect;
    viewport[0] *= mul;
    viewport[2] *= mul;
  } else {
    float mul = fake_aspect / real_aspect;
    viewport[1] *= mul;
    viewport[3] *= mul;
  }
  UTIL_VEC4_COPY(game_state.hud_viewport, viewport);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(viewport[0], viewport[2] + viewport[0], viewport[1], viewport[3] + viewport[1], -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glDisable(GL_DEPTH_TEST);

  if(game_state.level != NULL) {
    entity_render_view(game_state.camera_ent);
  }

  if(game_state.level == NULL && game_state.current_level != 0) {
    hud_render_intermission();
  }

  if(game_state.level == NULL && game_state.current_level == 0) {
    hud_render_menu_bg();
  }
  if(global_state.menu_shown) {
    hud_render_menu();
  }
  if(global_state.show_fps) {
    vec4_t white = {1, 1, 1, 1};
    char s[128];
    sprintf(s, "%d FPS", (int) (1.0 / global_state.time_delta_real));
    hud_render_text(s, 0, 0, 1.0, 1.0, 0.25, white);
  }

  glEnable(GL_DEPTH_TEST);

  glPopMatrix();
}
