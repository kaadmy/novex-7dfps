
#pragma once

GLuint texture_load(const char *filename);
GLuint texture_load_with_size(const char *filename, int *out_width, int *out_height, bool clamp);
