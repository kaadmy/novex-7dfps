
#pragma once

#include "types.h"

extern game_state_t game_state;

void game_init(void);
void game_update(void);
void game_render(void);

void game_unload_level(void);
void game_load_level(int level);
