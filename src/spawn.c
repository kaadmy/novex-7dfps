
#include "local.h"

static bool allow_infighting(entity_t *a, entity_t *b) {
  return false;
}

static bool allow_target(entity_t *a, entity_t *b) {
  if(a == NULL || b == NULL) {
    return false;
  }
  if(a->team != b->team && a->team != TEAM_NEUTRAL && b->team != TEAM_NEUTRAL && a->team != TEAM_ROGUE && b->team != TEAM_ROGUE) {
    vec3_t pos;
    vec_t dist;

    vec3_t dir;
    UTIL_VEC3_SUB(dir, a->position, b->position);
    vec_t len = util_vec3_length(dir);
    vec3_t start;
    UTIL_VEC3_COPY(start, b->position);
    start[2] += 1;
    if(level_cast_ray(game_state.level, start, dir, &pos, &dist) == NULL) {
      return true;
    }
    if(dist > len) {
      return true;
    }
  }
  return false;
}

static bool player_query_key_for(int lock) {
  if(lock == 0) {
    return true;
  }
  if(game_state.level->player->inventory[INVENTORY_KEY_RED + (lock - 1)] > 0) {
    return true;
  }
  const char *strings[3] = {
                            "YOU NEED THE RED KEY",
                            "YOU NEED THE YELLOW KEY",
                            "YOU NEED THE BLUE KEY",
  };
  hud_log(strings[(lock - 1)]);
  return false;
}

static void spawn_explosion(entity_t *self, vec3_t pos, vec_t damage, vec_t radius, vec3_t normal) {
  for(int i = 0; i < game_state.level->n_entities; i++) {
    entity_t *ent = game_state.level->entities[i];
    if(ent->collision_type != ENT_COLLISION_TYPE_NONE && ent->collision_type != ENT_COLLISION_TYPE_TRIGGER && ent->collision_type != ENT_COLLISION_TYPE_CORPSE) {
      vec3_t diff;
      UTIL_VEC3_SUB(diff, ent->position, pos);
      diff[2] = 0.0;
      vec_t dist = util_vec3_length(diff) - ent->radius - self->radius;
      util_vec3_normalize(diff);
      vec_t knockback = (radius - dist) / radius;
      if(knockback > 1) {
        knockback = 1;
      }
      if(knockback > 0) {
        if(ent->collision_type != ENT_COLLISION_TYPE_STATIC) {
          UTIL_VEC3_MA(ent->velocity, diff, knockback * (500.0 / ent->mass));
        }
        if(ent->ondamage != NULL) {
          ent->ondamage(ent, self, knockback * damage);
        }
      }
    }
  }
  sound_play(game_state.sounds.explosion, pos[0], pos[1], pos[2]);
  vec4_t vars = {};
  for(int i = 0; i < 7; i++) {
    entity_t *explosion = entity_init(game_state.level, "explosion", pos, 0.0, false, false, vars);
    float spd = ((rand() % 128) / 128.0) * 12.0;
    level_add_entity(game_state.level, explosion);
    explosion->velocity[0] += normal[0] * spd;
    explosion->velocity[1] += normal[1] * spd;
    explosion->velocity[2] += normal[2] * spd;
  }
}

static void spawn_gibs(vec3_t pos, int n) {
  vec4_t vars = {};

  for(int i = 0; i < n + (n * 3 * game_state.jodmode); i++) {
    entity_t *gib = entity_init(game_state.level, "gib", pos, 0.0, false, false, vars);
    level_add_entity(game_state.level, gib);
  }
}

static void spawn_gibs_blue(vec3_t pos, int n) {
  vec4_t vars = {1};

  for(int i = 0; i < n + (n * 3 * game_state.jodmode); i++) {
    entity_t *gib = entity_init(game_state.level, "gib", pos, 0.0, false, false, vars);
    level_add_entity(game_state.level, gib);
  }
}

static void spawn_casing(vec3_t pos, vec3_t vel) {
  vec4_t vars = {};
  entity_t *casing = entity_init(game_state.level, "casing", pos, 0.0, false, false, vars);
  UTIL_VEC3_COPY(casing->velocity, vel);
  casing->velocity[0] += ((rand() % 128) / 128.0) * 2.0 - 1.0;
  casing->velocity[1] += ((rand() % 128) / 128.0) * 2.0 - 1.0;
  casing->velocity[2] += ((rand() % 128) / 128.0) * 2.0 - 1.0;
  level_add_entity(game_state.level, casing);
}

static vec_t autoaim(entity_t *from, float h) {
  vec3_t start, dir = {0, 0, 0};
  UTIL_VEC3_COPY(start, from->position);
  start[2] += 1.0;

  vec_t s = sin(from->angle);
  vec_t c = cos(from->angle);
  dir[0] -= s * 1024.0;
  dir[1] += c * 1024.0;

  vec3_t hit;
  vec_t dist;
  entity_t *ent = NULL;
  int tries = 0;
  vec_t total_dist = 0;
  do {
    ent = level_cast_ray_ent_no_z(game_state.level, start, dir, &hit, &dist, from);
    if(ent == NULL) {
      break;
    }
    hit[2] = ent->position[2];

    vec3_t check_start, check_dir;
    check_start[2] += h;
    hit[2] += ent->height * 0.5;
    UTIL_VEC3_COPY(check_start, start);
    UTIL_VEC3_SUB(check_dir, hit, start);
    vec3_t tri_hit;
    vec_t tri_dist;
    level_triangle_t *tri = level_cast_ray(game_state.level, check_start, check_dir, &tri_hit, &tri_dist);

    total_dist += dist;
    if((tri != NULL && tri_dist < dist) || (ent->team == TEAM_NEUTRAL) || (ent->team == from->team) || ent == from) {
      UTIL_VEC3_MA(start, dir, dist + 0.001);
      ent = NULL;
    }
    tries++;
  } while(ent == NULL && tries < 10);
  if(ent != NULL) {
    vec3_t dist_2;
    UTIL_VEC3_SUB(dist_2, from->position, ent->position);
    dist_2[2] = 0.0;
    float dist = util_vec3_length(dist_2);
    float aim = ((ent->position[2] + ent->height * 0.5) - (from->position[2] + h));
    float ofs = ent->position[2] - from->position[2];
    if(aim > -0.15 && aim < 0.15) {
      return 0.0;
    }
    if(!strcmp(from->classname, "player")) {
      from->target = ent;
    }
    return aim / dist;
  }
  return 0.0;
}

static void shoot_bullet(vec_t spread, vec_t damage, entity_t *from) {
  vec_t z_ofs = autoaim(from, 1.0);
  vec3_t start, dir = {0, 0, 0};
  UTIL_VEC3_COPY(start, from->position);
  start[2] += 1.0;

  vec_t s = sin(from->angle);
  vec_t c = cos(from->angle);
  dir[0] -= s * 1024.0;
  dir[1] += c * 1024.0;

  vec_t rnd = ((rand() % 128) - 64) * spread;
  vec_t rnd2 = ((rand() % 128) - 64) * spread;
  dir[0] += c * rnd;
  dir[1] += s * rnd;
  dir[2] += rnd2 * 0.3;
  dir[2] += z_ofs * 1024.0;

  vec3_t hit;
  level_triangle_t *tri;
  vec_t dist;

  vec3_t hit2;
  entity_t *ent;
  vec_t dist2;

  tri = level_cast_ray(game_state.level, start, dir, &hit, &dist);
  ent = level_cast_ray_ent(game_state.level, start, dir, &hit2, &dist2, from);

  if(tri == NULL || (ent != NULL && dist2 < dist)) {
    if(ent == NULL) {
      return;
    }

    dist = dist2;
    UTIL_VEC3_COPY(hit, hit2);

    if(ent->ondamage != NULL) {
      ent->ondamage(ent, from, damage);
    }
    if(ent->spawn_gibs) {
      //spawn_gibs(hit, 1);
    }
  }
  vec4_t vars = {};
  entity_t *sparks = entity_init(game_state.level, "sparks", hit, 0.0, false, false, vars);
  level_add_entity(game_state.level, sparks);
  level_collide_walls(game_state.level, sparks, sparks->position, 0.2, 0.0, NULL);
}

/** entitydef: player **/
static void player_spawn(entity_t *ent) {
  ent->previous_weapon = 0;
  ent->weapon = 0;
  ent->mass = 100;
  ent->radius = 0.3;
  ent->height = 1.2;
  ent->team = TEAM_PLAYER;
  ent->spawn_gibs = true;

  ent->speed = 50.0;
  ent->friction = 0.0005;
  ent->collision_type = ENT_COLLISION_TYPE_DYNAMIC;
  //sprite_load("floor01.png", &ent->sprite);
  sprite_load("sprites/weapon_shotgun.png", &ent->viewsprites[WEAPON_SHOTGUN]);
  sprite_load("sprites/weapon_supershotgun.png", &ent->viewsprites[WEAPON_SUPERSHOTGUN]);
  sprite_load("sprites/weapon_minigun.png", &ent->viewsprites[WEAPON_MINIGUN]);
  sprite_load("sprites/weapon_rocketlauncher.png", &ent->viewsprites[WEAPON_ROCKETLAUNCHER]);
  ent->viewsprites[WEAPON_SHOTGUN].hframes = 14;
  ent->viewsprites[WEAPON_SUPERSHOTGUN].hframes = 18;
  ent->viewsprites[WEAPON_MINIGUN].hframes = 4;
  ent->viewsprites[WEAPON_ROCKETLAUNCHER].hframes = 8;
  memcpy(&ent->viewsprite, &ent->viewsprites[0], sizeof(sprite_t));
  ent->state[0] = 0.0;
  ent->state[1] = 0.0;
  ent->state[2] = 0.0;
  ent->inventory[INVENTORY_HEALTH] = 100;
  ent->inventory[INVENTORY_ARMOR] = 0;

  ent->inventory[INVENTORY_SHELLS] = 10;
  ent->inventory[INVENTORY_SHOTGUN] = 1;

  ent->is_player = true;
}


static void player_update(entity_t *ent) {
  ent->target = NULL;
  autoaim(ent, 1.0);
  ent->state[2] -= game_state.time_delta * 15.0;
  ent->state[3] -= game_state.time_delta * 5.0;
  if(ent->state[3] > 0) {
    ent->viewtint[0] = 1.0;
    ent->viewtint[1] = 1.0;
    ent->viewtint[2] = 0.7;
    ent->viewtint[3] = UTIL_CLAMP(ent->state[3] * 0.2, 0.0, 0.7);
  } else if(ent->state[2] > 0) {
    ent->viewtint[0] = 1.0;
    ent->viewtint[1] = 0.0;
    ent->viewtint[2] = 0.0;
    ent->viewtint[3] = UTIL_CLAMP(ent->state[2] * 0.1, 0.0, 0.7);
  } else {
    ent->viewtint[0] = 1.0;
    ent->viewtint[1] = 1.0;
    ent->viewtint[2] = 1.0;
    ent->viewtint[3] = 0.0;
  }
  if(ent->state[2] < 0) {
    ent->state[2] = 0;
  }
  if(ent->weapon != ent->previous_weapon) {
    if(!ent->inventory[INVENTORY_SHOTGUN + ent->weapon]) {
      ent->weapon = ent->previous_weapon;
    } else {
      ent->state[1] = 0.0;
      memcpy(&ent->viewsprite, &ent->viewsprites[ent->weapon], sizeof(sprite_t));
      ent->previous_weapon = ent->weapon;
    }
  }
  ent->state[1] += game_state.time_delta;
  if(ent->state[1] < 0.3) {
    ent->bob_amount = 0;
    ent->viewsprite.offset[1] = pow((ent->state[1] - 0.3) / 0.3, 3.0) * 20.0;
    return;
  }
  ent->viewsprite.offset[0] = 0;
  ent->viewsprite.offset[1] = 0;
  if(ent->weapon == WEAPON_SHOTGUN) {
    if(ent->viewsprite.frame != 0) {
      while(ent->viewsprite.animation_time > 0.05) {
        ent->viewsprite.animation_time -= 0.05;
        ent->viewsprite.frame++;
        if(ent->viewsprite.frame == 9) {
          vec3_t pos, vel;
          UTIL_VEC3_COPY(pos, ent->position);
          UTIL_VEC3_COPY(vel, ent->velocity);
          pos[0] -= sin(ent->angle) * 0.8;
          pos[1] += cos(ent->angle) * 0.8;
          pos[2] += 0.6;
          vel[0] += cos(ent->angle) * 2.0;
          vel[1] += sin(ent->angle) * 2.0;
          vel[2] += 2.5;
          spawn_casing(pos, vel);
        }
        if(ent->viewsprite.frame > 13) {
          ent->viewsprite.frame = 0;
        }
      }
    } else {
      ent->viewsprite.animation_time = 0;
    }
    if(ent->controls & CONTROL_SHOOT && ent->state[0] > 0.0 && ent->inventory[INVENTORY_SHELLS] > 0) {
      ent->inventory[INVENTORY_SHELLS]--;
      ent->state[0] = -0.7;
      ent->viewsprite.frame = 1;
      ent->viewsprite.animation_time = 0;

      for(int i = 0; i < 5; i++) {
        shoot_bullet(1.3, 3.0, ent);
      }

      sound_play_global(game_state.sounds.shotgun_fire);
    }
    ent->state[0] += game_state.time_delta;
  } else if(ent->weapon == WEAPON_SUPERSHOTGUN) {
    if(ent->viewsprite.frame != 0) {
      while(ent->viewsprite.animation_time > 0.05) {
        ent->viewsprite.animation_time -= 0.05;
        ent->viewsprite.frame++;
        if(ent->viewsprite.frame == 12) {
          vec3_t pos, vel;
          UTIL_VEC3_COPY(pos, ent->position);
          UTIL_VEC3_COPY(vel, ent->velocity);
          pos[0] -= sin(ent->angle) * 0.8;
          pos[1] += cos(ent->angle) * 0.8;
          pos[2] += 0.6;
          vel[0] += cos(ent->angle) * 2.0;
          vel[1] += sin(ent->angle) * 2.0;
          vel[2] += 2.5;
          spawn_casing(pos, vel);
          vel[0] -= cos(ent->angle) * 4.0;
          vel[1] -= sin(ent->angle) * 4.0;
          spawn_casing(pos, vel);
        }
        if(ent->viewsprite.frame > 17) {
          ent->viewsprite.frame = 0;
        }
      }
    } else {
      ent->viewsprite.animation_time = 0;
    }
    if(ent->controls & CONTROL_SHOOT && ent->state[0] > 0.0 && ent->inventory[INVENTORY_SHELLS] > 1) {
      ent->inventory[INVENTORY_SHELLS] -= 2;
      ent->state[0] = -1.3;
      ent->viewsprite.frame = 1;
      ent->viewsprite.animation_time = 0;

      for(int i = 0; i < 18; i++) {
        shoot_bullet(3.8, 6.0, ent);
      }

      sound_play_global(game_state.sounds.shotgun_fire);
    }
    ent->state[0] += game_state.time_delta;
  } else if(ent->weapon == WEAPON_MINIGUN) {
    if(ent->viewsprite.frame != 0) {
      while(ent->viewsprite.animation_time > 0.03) {
        ent->viewsprite.animation_time -= 0.03;
        ent->viewsprite.frame++;
        if(ent->viewsprite.frame > 3) {
          ent->viewsprite.frame = 0;
        }
      }
    } else {
      ent->viewsprite.animation_time = 0;
    }
    if(ent->controls & CONTROL_SHOOT && ent->state[0] > 0 && ent->inventory[INVENTORY_BULLETS] > 0) {
      vec3_t pos, vel;
      UTIL_VEC3_COPY(pos, ent->position);
      UTIL_VEC3_COPY(vel, ent->velocity);
      pos[0] -= sin(ent->angle) * 0.8;
      pos[1] += cos(ent->angle) * 0.8;
      pos[2] += 0.6;
      vel[0] += cos(ent->angle) * 6.0;
      vel[1] += sin(ent->angle) * 6.0;
      vel[2] += 1.0;
      spawn_casing(pos, vel);

      ent->inventory[INVENTORY_BULLETS]--;
      ent->state[0] = -0.09;
      ent->viewsprite.frame = 1;
      ent->viewsprite.animation_time = 0;
      shoot_bullet(0.4, 8.0, ent);
      sound_play_global(game_state.sounds.minigun_fire);
    }
    ent->state[0] += game_state.time_delta;
  } else if(ent->weapon == WEAPON_ROCKETLAUNCHER) {
    if(ent->viewsprite.frame != 0) {
      while(ent->viewsprite.animation_time > 0.05) {
        ent->viewsprite.animation_time -= 0.05;
        ent->viewsprite.frame++;
        if(ent->viewsprite.frame > 7) {
          ent->viewsprite.frame = 0;
        }
      }
    } else {
      ent->viewsprite.animation_time = 0;
    }
    if(ent->controls & CONTROL_SHOOT && ent->state[0] > 0.0 && ent->inventory[INVENTORY_ROCKETS] > 0) {
      ent->inventory[INVENTORY_ROCKETS]--;
      ent->state[0] = -0.6;
      ent->viewsprite.frame = 1;
      ent->viewsprite.animation_time = 0;
      vec4_t vars = {};
      vec3_t pos;
      UTIL_VEC3_COPY(pos, ent->position);
      pos[2] += 0.7;
      entity_t *rocket = entity_init(game_state.level, "rocket", pos, ent->angle, false, false, vars);
      rocket->owner = ent;
      level_add_entity(game_state.level, rocket);
      sound_play_global(game_state.sounds.rocketlauncher_fire);
    }
    ent->state[0] += game_state.time_delta;
  }
  ent->viewsprite.animation_time += game_state.time_delta;
  ent->bob_amount = util_vec3_length(ent->velocity) * 0.1;
  ent->view_height = cos(game_state.time_elapsed * 8.0) * ent->bob_amount * 0.2 + 1.0;
  if(ent->state[0] < 0) {
    ent->bob_amount = 0;
  }
}

static void player_update_dead(entity_t *self) {
  self->view_height -= (self->view_height - 0.2) * (1.0 - pow(0.0001, game_state.time_delta));
  self->speed = 0.0;
  self->viewsprite.texture = 0;
  self->collision_type = ENT_COLLISION_TYPE_CORPSE;
  self->team = TEAM_NEUTRAL;
  self->viewtint[0] = 1.0;
  self->viewtint[1] = 0.0;
  self->viewtint[2] = 0.0;
  self->viewtint[3] = 0.5;
  self->state[0] += game_state.time_delta;
}

static void player_input(entity_t *ent, int controls, vec2_t aim) {
  if(ent->inventory[INVENTORY_HEALTH] <= 0) {
    if(controls & CONTROL_USE && ent->state[0] > 1.0) {
      game_state.warp_to_level = game_state.current_level;
      game_state.intermission_type = INTERMISSION_TYPE_NONE;
    }
  } else {
    ent->controls = controls;
    ent->angle += aim[0] * pow(game_state.sensitivity + 1, 1.3) * 0.2;
    //ent->angle_y += aim[1] * pow(game_state.sensitivity + 1, 1.3) * 0.2;
  }
}

static void player_damage(entity_t *ent, entity_t *other, vec_t dmg_f) {
  int dmg = dmg_f;
  ent->state[2] += dmg;
  if(ent->state[2] > 10) {
    ent->state[2] = 10;
  }
  if(ent->inventory[INVENTORY_ARMOR] > 0) {
    ent->inventory[INVENTORY_ARMOR] -= dmg;
    if(ent->inventory[INVENTORY_ARMOR] < 0) {
      dmg = -ent->inventory[INVENTORY_ARMOR];
      ent->inventory[INVENTORY_ARMOR] = 0;
    } else {
      dmg = 0;
    }
  }

  if(!game_state.jodmode) {
    ent->inventory[INVENTORY_HEALTH] -= dmg;
  }

  if(ent->inventory[INVENTORY_HEALTH] <= 0) {
    ent->onupdate = &player_update_dead;
    ent->ondamage = NULL;
    ent->onpickup = NULL;
    ent->state[0] = 0.0;
  }
}

static void player_pickup(entity_t *self, int item, int amt) {
  static int maximums[] = {
                           60, // SHELLS
                           200, // BULLETS
                           40, // ROCKETS
                           1, // SHOTGUN
                           1, // SUPER SHOTGUN
                           1, // MINIGUN
                           1, // ROCKETLAUNCHER
                           100, // HEALTH
                           100, // ARMOR
                           1, 1, 1, // keys
                           100, // COLLECTIBLES
  };

  if(item == INVENTORY_SHOTGUN) {
    player_pickup(self, INVENTORY_SHELLS, 6);
  }
  if(item == INVENTORY_SUPERSHOTGUN) {
    player_pickup(self, INVENTORY_SHELLS, 12);
  }
  if(item == INVENTORY_MINIGUN) {
    player_pickup(self, INVENTORY_BULLETS, 25);
  }
  if(item == INVENTORY_ROCKETLAUNCHER) {
    player_pickup(self, INVENTORY_ROCKETS, 5);
  }
  if(item >= INVENTORY_SHOTGUN && item <= INVENTORY_ROCKETLAUNCHER) {
    if(!self->inventory[item]) {
      self->weapon = item - INVENTORY_SHOTGUN + WEAPON_SHOTGUN;
    }
  }

  int max = maximums[item];

  self->inventory[item] = UTIL_CLAMP(self->inventory[item] + amt, 0, max);
  self->state[3] = 1.0;

  if(item == INVENTORY_ARMOR) {
    sound_play_global(game_state.sounds.pickup_armor);
  } else if(item == INVENTORY_HEALTH) {
    sound_play_global(game_state.sounds.pickup_health);
  } else if(item == INVENTORY_COLLECTIBLE) {
    sound_play_global(game_state.sounds.pickup_collectible);
  } else if(item == INVENTORY_SHOTGUN || item == INVENTORY_SUPERSHOTGUN || item == INVENTORY_MINIGUN || item == INVENTORY_ROCKETLAUNCHER) {
    sound_play_global(game_state.sounds.pickup_weapon);
  } else {
    sound_play_global(game_state.sounds.pickup_item);
  }
}

static void player_init(entity_t *ent) {
  ent->onspawn = player_spawn;
  ent->onupdate = player_update;
  ent->ondamage = player_damage;
  ent->onpickup = player_pickup;
  ent->oninput = player_input;
}

/** entitydef: rocket **/

static void rocket_update(entity_t *ent) {
  if(ent->state[0] > 0.03) {
    vec4_t vars = {};
    entity_t *sparks = entity_init(game_state.level, "sparks", ent->position, 0.0, false, false, vars);
    level_add_entity(game_state.level, sparks);
    ent->state[0] = 0;
  }
  ent->state[0] += game_state.time_delta;
  if(ent->state[1] > 3) {
    level_remove_entity(game_state.level, ent);
  }
  ent->state[1] += game_state.time_delta;
}

static void rocket_spawn(entity_t *ent) {
  sprite_load("sprites/object_rocket.png", &ent->sprite);
  ent->collision_type = ENT_COLLISION_TYPE_TRIGGER;
  ent->z_offset = 3;
  ent->radius = 0.18;
  ent->height = 0.2;
  ent->step_height = 0.0;
  ent->floating = true;
  ent->velocity[0] = -sin(ent->angle) * 15.0;
  ent->velocity[1] = cos(ent->angle) * 15.0;
  ent->velocity[2] = autoaim(ent->owner, 0.8) * 15.0;
  ent->friction = 1.0;
  ent->inventory[INVENTORY_HEALTH] = 1;
}

static void rocket_collideent(struct entity_s *self, struct entity_s *other) {
  if(self->inventory[INVENTORY_HEALTH] > 0) {
    if(other != self->owner) {
      vec3_t zero = {};
      self->inventory[INVENTORY_HEALTH] = 0;
      spawn_explosion(self->owner, self->position, 40.0, 2.0, zero);
      level_remove_entity(game_state.level, self);
    }
  }
}

static void rocket_collidetri(struct entity_s *self, level_triangle_t *other, vec3_t normal) {
  if(self->inventory[INVENTORY_HEALTH] > 0) {
    spawn_explosion(self, self->position, 40.0, 2.0, normal);
    level_remove_entity(game_state.level, self);
    self->inventory[INVENTORY_HEALTH] = 0;
  }
}

static void rocket_init(entity_t *ent) {
  ent->onspawn = rocket_spawn;
  ent->onupdate = rocket_update;
  ent->oncollideent = rocket_collideent;
  ent->oncollidetri = rocket_collidetri;

}
/** entitydef: sparks **/

static void sparks_spawn(entity_t *ent);
static void sparks_update(entity_t *ent);

static void sparks_init(entity_t *ent) {
  ent->onspawn = sparks_spawn;
  ent->onupdate = sparks_update;
}

static void sparks_spawn(entity_t *ent) {
  sprite_load("sprites/sparks.png", &ent->sprite);
  ent->sprite.rotation = (rand() % 4) * 90;
  ent->collision_type = ENT_COLLISION_TYPE_NONE;
  ent->radius = 0.1;
  ent->height = 0.2;
  ent->floating = true;
  ent->sprite.hframes = 6;
  ent->z_offset = 3;
  ent->sprite.frame = rand() % 2;
}

static void sparks_update(entity_t *ent) {
  ent->state[0] += game_state.time_delta;
  if(ent->state[0] > 0.05) {
    ent->sprite.frame = ((ent->state[0] - 0.05) / 0.1) + 2;
  }
  if(ent->state[0] > 0.6) {
    level_remove_entity(game_state.level, ent);
  }
}

/** entitydef: explosion **/

static void explosion_spawn(entity_t *ent);
static void explosion_update(entity_t *ent);

static void explosion_init(entity_t *ent) {
  ent->onspawn = explosion_spawn;
  ent->onupdate = explosion_update;
}

static void explosion_spawn(entity_t *ent) {
  sprite_load("sprites/explosion.png", &ent->sprite);
  ent->sprite.rotation = (rand() % 4) * 90;
  ent->collision_type = ENT_COLLISION_TYPE_NONE;
  ent->radius = 0.1;
  ent->height = 0.2;
  ent->floating = true;
  ent->sprite.hframes = 8;
  ent->z_offset = 6;
  ent->velocity[0] = ((rand() % 128) / 128.0 - 0.5) * 9.0;
  ent->velocity[1] = ((rand() % 128) / 128.0 - 0.5) * 9.0;
  ent->velocity[2] = ((rand() % 128) / 128.0 - 0.5) * 9.0;
  ent->friction = 0.01;
}

static void explosion_update(entity_t *ent) {
  ent->state[0] += game_state.time_delta;
  ent->sprite.frame = ent->state[0] / 0.05;
  if(ent->state[0] > 0.05 * 8) {
    level_remove_entity(game_state.level, ent);
  }
}

/** entitydef: gib **/

static void gib_spawn(entity_t *ent);
static void gib_update(entity_t *ent);

static void gib_init(entity_t *ent) {
  ent->onspawn = gib_spawn;
  ent->onupdate = gib_update;
}

static void gib_spawn(entity_t *ent) {
  if(ent->customvars[0] > 0.5) {
    sprite_load("sprites/gibs_blue.png", &ent->sprite);
  } else {
    sprite_load("sprites/gibs.png", &ent->sprite);
  }
  ent->gravity = 0.7;
  ent->state[1] = 0.0;
  ent->state[2] = (rand() % 128) / 512.0;
  ent->state[3] = (rand() % 2) ? -1 : 1;
  ent->sprite.rotation = (rand() % 4) * 90;
  ent->sprite.frame = (rand() % 6);
  ent->collision_type = ENT_COLLISION_TYPE_NONE;
  ent->radius = 0.1;
  ent->height = 0.2;
  ent->bounce = 0.6;
  ent->sprite.hframes = 6;
  ent->z_offset = 10;
  ent->position[2] += 0.5;
  ent->velocity[0] = ((rand() % 128) / 128.0 - 0.5) * 12.0;
  ent->velocity[1] = ((rand() % 128) / 128.0 - 0.5) * 12.0;
  ent->velocity[2] = ((rand() % 128) / 128.0 - 0.5) * 12.0;
  ent->friction = 0.05;
}

static void gib_update(entity_t *ent) {
  ent->state[0] += game_state.time_delta;
  ent->state[1] += game_state.time_delta;
  ent->state[2] += game_state.time_delta * 0.1;
  if(ent->state[1] > ent->state[2]) {
    ent->state[1] = 0;
    ent->sprite.rotation += ent->state[3] * 90;
  }
  if(ent->state[0] > 2.0) {
    level_remove_entity(game_state.level, ent);
  }
}

/** entitydef: gib **/

static void casing_spawn(entity_t *ent);
static void casing_update(entity_t *ent);

static void casing_init(entity_t *ent) {
  ent->onspawn = casing_spawn;
  ent->onupdate = casing_update;
}

static void casing_spawn(entity_t *ent) {
  sprite_load("sprites/casing.png", &ent->sprite);
  ent->gravity = 0.7;
  ent->state[0] = 0.0;
  ent->state[1] = rand() % 2 < 1 ? -1 : 1;
  ent->state[2] = 0.03;
  ent->sprite.rotation = (rand() % 4) * 90;
  ent->sprite.frame = (rand() % 2);
  ent->collision_type = ENT_COLLISION_TYPE_NONE;
  ent->step_height = 0.01;
  ent->radius = 0.1;
  ent->height = 0.2;
  ent->bounce = 0.7;
  ent->sprite.hframes = 2;
  ent->friction = 0.05;
}

static void casing_update(entity_t *ent) {
  ent->state[2] += game_state.time_delta * 0.2;
  ent->state[3] += game_state.time_delta;
  ent->state[0] += game_state.time_delta;
  if(ent->state[0] > ent->state[2]) {
    ent->state[0] -= ent->state[2];
    ent->sprite.frame += ent->state[1];
    if(ent->sprite.frame > 1 || ent->sprite.frame < 0) {
      ent->sprite.frame = ent->state[1] < 0 ? 1 : 0;
      ent->sprite.rotation += ent->state[1] * 90;
    }
  }
  if(ent->state[3] > 2.0) {
    level_remove_entity(game_state.level, ent);
  }
}

/* entitydef: func_door */

static void func_door_spawn(entity_t *self) {
  self->sounds[0] = 0;
  if(self->startbackwards) {
    self->state[0] = self->customvars[0];
    self->enabled = true;
  } else {
    self->state[0] = 0.0;
  }
  self->floating = true;
}

static bool func_door_squish(entity_t *self, entity_t *other) {
  if(self->customvars[3] > -0.5 && self->customvars[3] < 0.5) {
    self->enabled = true;
    self->state[1] = 0.0;
    return false;
  }
  return true;
}

static void func_door_update(entity_t *self) {
  bool toggle = false;
  for(int i = 0; i < self->n_triangles; i++) {
    if(self->triangles[i]->used) {
      self->triangles[i]->used = false;
      if(self->remote) {
        hud_log("THIS DOOR IS LOCKED FROM ELSEWHERE");
      } else {
        self->activator = self->triangles[i]->activator;
        toggle = true;
      }
    }
  }
  if(toggle) {
    if(player_query_key_for(self->lock)) {
      self->ontrigger(self, self->activator);
    }
  }
  bool play_sounds = true;
  if(self->customvars[3] < -0.5) {
    play_sounds = false;
  }
  vec_t spd = fabs(self->customvars[1]);
  vec_t prev_cv0 = self->state[0];
  if(self->enabled) {
    self->state[0] += game_state.time_delta * spd;
    if(self->state[0] >= self->customvars[0] && prev_cv0 < self->customvars[0]) {
      if(play_sounds) {
        sound_play(game_state.sounds.door_start, self->position[0], self->position[1], self->position[2]);
      }
    }
    self->state[1] += game_state.time_delta;
    if(self->state[1] > self->customvars[2] && !self->startbackwards && self->customvars[2] > 0) {
      if(play_sounds) {
        sound_play(game_state.sounds.door_start, self->position[0], self->position[1], self->position[2]);
      }
      self->enabled = false;
    }
  } else {
    if(self->customvars[3] < -0.5) {
      self->state[0] -= game_state.time_delta * spd * self->state[2];
      self->state[2] += game_state.time_delta;
    } else {
      self->state[0] -= game_state.time_delta * spd;
    }
    if(self->state[0] <= 0 && prev_cv0 > 0) {
      if(self->target != NULL && self->target->ontrigger != NULL) {
        self->target->ontrigger(self->target, self->activator);
      }
      if(play_sounds) {
        sound_play(game_state.sounds.door_start, self->position[0], self->position[1], self->position[2]);
      }
    }
    self->state[1] += game_state.time_delta;
    if(self->state[1] > self->customvars[2] && self->startbackwards && self->customvars[2] > 0) {
      if(play_sounds) {
        sound_play(game_state.sounds.door_start, self->position[0], self->position[1], self->position[2]);
      }
      self->enabled = true;
    }
  }
  self->state[0] = UTIL_CLAMP(self->state[0], 0.0, self->customvars[0]);
  if(play_sounds) {
    if(self->state[0] != prev_cv0) {
      if(self->sounds[0] == 0) {
        self->sounds[0] = sound_play(game_state.sounds.door_move, self->position[0], self->position[1], self->position[2]);
        alSourcei(self->sounds[0], AL_LOOPING, AL_TRUE);
      } else {
        alSourcef(self->sounds[0], AL_PITCH, (self->state[0] / self->customvars[0]) * 1.2 + 0.4);
      }
    } else {
      if(self->sounds[0] != 0) {
        alSourceStop(self->sounds[0]);
        self->sounds[0] = 0;
      }
    }
  }
  for(int i = 0; i < self->n_triangles; i++) {
    for(int j = 0; j < 3; j++) {
      self->triangles[i]->vertices[j].pos[2] = self->state[0] + self->triangles[i]->vertices[j].startpos[2];
    }
  }
}

static void func_door_trigger(entity_t *self, entity_t *activator) {
  self->activator = activator;
  sound_play(game_state.sounds.door_start, self->position[0], self->position[1], self->position[2]);
  self->enabled = !self->enabled;
  self->state[1] = 0;
}

static void func_door_init(entity_t *ent) {
  ent->onspawn = &func_door_spawn;
  ent->onupdate = &func_door_update;
  ent->ontrigger = &func_door_trigger;
  ent->onsquish = &func_door_squish;
}

/** entitydef: func_switch **/

static void func_switch_spawn(entity_t *ent) {
  ent->floating = true;
}

static void func_switch_update(entity_t *ent) {
  bool was_enabled = ent->enabled;
  bool toggle = false;
  if(!was_enabled) {
    for(int i = 0; i < ent->n_triangles; i++) {
      if(ent->triangles[i]->used) {
        ent->triangles[i]->used = false;
        if(ent->target != NULL && ent->target->ontrigger != NULL) {
          if(player_query_key_for(ent->lock)) {
            ent->target->ontrigger(ent->target, ent->triangles[i]->activator);
            ent->state[0] = 0;
            ent->enabled = true;
            toggle = true;
          } else {
            sound_play(game_state.sounds.switch_locked, ent->position[0], ent->position[1], ent->position[2]);
          }
        }
      }
    }
  }
  ent->state[0] += game_state.time_delta;
  if(ent->state[0] > 0.5 && ent->customvars[0] < 0.5 && ent->enabled) {
    ent->enabled = false;
  }

  if(was_enabled != ent->enabled) {
    if(toggle) {
      sound_play(game_state.sounds.switch_activate, ent->position[0], ent->position[1], ent->position[2]);
    } else {
      sound_play(game_state.sounds.switch_return, ent->position[0], ent->position[1], ent->position[2]);
    }
    vec_t uv_offset = was_enabled ? -0.5 : 0.5;
    for(int i = 0; i < ent->n_triangles; i++) {
      for(int j = 0; j < 3; j++) {
        ent->triangles[i]->vertices[j].uv[0] += uv_offset;
      }
    }
  }
}

static void func_switch_init(entity_t *ent) {
  ent->onspawn = &func_switch_spawn;
  ent->onupdate = &func_switch_update;
}

/** entitydef: prop_barrel **/

static void prop_barrel_spawn(entity_t *ent) {
  sprite_load("sprites/prop_barrel.png", &ent->sprite);
  ent->collision_type = ENT_COLLISION_TYPE_DYNAMIC;

  ent->team = TEAM_ROGUE;
  ent->inventory[INVENTORY_HEALTH] = 10;
  ent->mass = 100;

  ent->radius = 0.3;
  ent->height = 0.7;
}

static void prop_barrel_update(entity_t *ent) {
  if(ent->state[0] > 0.5) {
    ent->state[1] += game_state.time_delta;
    if(ent->state[1] > 0.1) {
      vec3_t pos;
      UTIL_VEC3_COPY(pos, ent->position);
      pos[2] += 0.7;
      vec3_t zero = {};
      spawn_explosion(ent, pos, 40.0, 2.0, zero);
      level_remove_entity(game_state.level, ent);
    }
  }
}

static void prop_barrel_damage(entity_t *ent, entity_t *other, vec_t dmg) {
  ent->inventory[INVENTORY_HEALTH] -= dmg;
  if(ent->inventory[INVENTORY_HEALTH] < 0) {
    ent->state[0] = 1;
  }
}

static void prop_barrel_init(entity_t *ent) {
  ent->onspawn = &prop_barrel_spawn;
  ent->onupdate = &prop_barrel_update;
  ent->ondamage = &prop_barrel_damage;
}

/** entitydef: item_key **/

static void item_key_spawn(entity_t *ent) {
  if(ent->lock == 0) ent->lock = 1; // fallback to redk ey
  sprite_load("sprites/keys.png", &ent->sprite);
  ent->sprite.hframes = 6;
  ent->sprite.frame = (ent->lock - 1) * 2;
  ent->collision_type = ENT_COLLISION_TYPE_TRIGGER;
  ent->radius = 0.6;
  ent->height = 0.3;
  ent->state[1] = 1;
}

static void item_key_update(entity_t *ent) {
  ent->state[0] += game_state.time_delta;
  while(ent->state[0] > 0.2) {
    ent->sprite.frame += ent->state[1];
    ent->state[1] *= -1;
    ent->state[0] -= 0.2;
  }
}

static void item_key_collideent(entity_t *self, entity_t *other) {
  if(other->is_player && other->onpickup != NULL) {
    other->onpickup(other, INVENTORY_KEY_RED + self->lock - 1, 1);
    level_remove_entity(game_state.level, self);
    const char *texts[3] = {
                            "YOU GOT THE RED KEY",
                            "YOU GOT THE YELLOW KEY",
                            "YOU GOT THE BLUE KEY"
    };
    vec4_t colors[3] = {
                        {0.9, 0.12, 0.22, 1.0},
                        {0.85, 0.77, 0.22, 1.0},
                        {0.09, 0.68, 1.98, 1.0},
    };
    hud_log_color(texts[self->lock - 1], colors[self->lock - 1]);
    if(self->target != NULL && self->target->ontrigger != NULL) {
      self->target->ontrigger(self->target, other);
    }
  }
}

static void item_key_init(entity_t *ent) {
  ent->onspawn = &item_key_spawn;
  ent->onupdate = &item_key_update;
  ent->oncollideent = &item_key_collideent;
}

/** entitydef: item_collectible **/

static void item_collectible_spawn(entity_t *ent) {
  sprite_load("sprites/collectible.png", &ent->sprite);
  ent->sprite.hframes = 4;
  ent->sprite.frame = 0;
  ent->collision_type = ENT_COLLISION_TYPE_TRIGGER;
  ent->radius = 0.6;
  ent->height = 0.3;
  game_state.level_stats.collectibles_total++;
}

static void item_collectible_update(entity_t *ent) {
  ent->state[0] += game_state.time_delta;

  while(ent->state[0] > 0.2) {
    ent->sprite.frame += 1;
    ent->state[0] -= 0.2;

    if(ent->sprite.frame > 3) {
      ent->sprite.frame = 0;
    }
  }
}

static void item_collectible_collideent(entity_t *self, entity_t *other) {
  if(other->is_player && other->onpickup != NULL) {
    other->onpickup(other, INVENTORY_COLLECTIBLE, 1);
    level_remove_entity(game_state.level, self);
    vec4_t color = {1.0, 1.0, 1.0, 1.0};
    hud_log_color("YOU PICKED UP A LEGENDARY HELMET", color);
    game_state.level_stats.collectibles++;
    if(self->target != NULL && self->target->ontrigger != NULL) {
      self->target->ontrigger(self->target, other);
    }
  }
}

static void item_collectible_init(entity_t *ent) {
  ent->onspawn = &item_collectible_spawn;
  ent->onupdate = &item_collectible_update;
  ent->oncollideent = &item_collectible_collideent;
}

/** entitydef: trigger_changelevel **/

static void trigger_changelevel_trigger(entity_t *self, entity_t *other) {
  game_state.warp_to_level = self->customvars[0] * 10 + self->customvars[1];;
  game_state.intermission_type = INTERMISSION_TYPE_NORMAL;
}

static void trigger_changelevel_init(entity_t *ent) {
  ent->ontrigger = trigger_changelevel_trigger;
}

/** entitydef: trigger_cylinder **/

// customvars: [radius, height, delay]

static void trigger_cylinder_spawn(entity_t *self) {
  self->radius = self->customvars[0];
  self->height = self->customvars[1];
  self->collision_type = ENT_COLLISION_TYPE_TRIGGER;
  self->nophys = true;
  self->floating = true;
  self->gravity = 0;
}

static void trigger_cylinder_update(entity_t *self) {
  if(self->state[1] > 0.5) {
    self->state[0] += game_state.time_delta;
    if(self->state[0] > self->customvars[2]) {
      if(self->target != NULL && self->target->ontrigger != NULL) {
        self->target->ontrigger(self->target, self->activator);
      }
      level_remove_entity(game_state.level, self);
    }
  }
}

static void trigger_cylinder_collideent(entity_t *self, entity_t *other) {
  if(self->customvars[3] > 0.5) {
    if(strcmp(other->classname, "player")) {
      return;
    }
  }
  if(self->remote) {
    self->target->ontrigger(self->target, other);
  } else {
    self->oncollideent = NULL;
    self->activator = other;
    self->state[1] = 1.0;
  }
}

static void trigger_cylinder_init(entity_t *ent) {
  ent->onspawn = &trigger_cylinder_spawn;
  ent->onupdate = &trigger_cylinder_update;
  ent->oncollideent = &trigger_cylinder_collideent;
}

/** entitydef: trigger_teleport_destination **/

static void trigger_teleport_destination_spawn(entity_t *ent) {
  ent->gravity = 0;
  ent->nophys = true;
  ent->floating = true;
  ent->collision_type = ENT_COLLISION_TYPE_NONE;
}

static void trigger_teleport_destination_trigger(entity_t *ent, entity_t *activator) {
  UTIL_VEC3_COPY(activator->position, ent->position);
  activator->angle = ent->angle;
  sound_play(game_state.sounds.teleport, ent->position[0], ent->position[1], ent->position[2]);
}

static void trigger_teleport_destination_init(entity_t *ent) {
  ent->onspawn = &trigger_teleport_destination_spawn;
  ent->ontrigger = &trigger_teleport_destination_trigger;
}

/** entitydef: trigger_counter **/

static void trigger_counter_spawn(entity_t *ent) {
  ent->gravity = 0;
  ent->nophys = true;
  ent->floating = true;
  ent->collision_type = ENT_COLLISION_TYPE_NONE;
}

static void trigger_counter_trigger(entity_t *ent, entity_t *activator) {
  ent->state[0]++;
  if(ent->state[0] > ent->customvars[0]) {
    ent->state[0] = -999999;
    if(ent->target != NULL && ent->target->ontrigger != NULL) {
      ent->target->ontrigger(ent->target, activator);
    }
  }
}

static void trigger_counter_init(entity_t *ent) {
  ent->onspawn = &trigger_counter_spawn;
  ent->ontrigger = &trigger_counter_trigger;
}

/* pickups */

#define CREATE_PICKUP(name, item, amt, txt, txt2) static void name##_spawn(entity_t *ent) { ent->height = 0.1; ent->radius = 0.6; sprite_load("sprites/" #name ".png", &ent->sprite); ent->collision_type = ENT_COLLISION_TYPE_TRIGGER; game_state.level_stats.items_total++; ent->friction = 0.1; ent->gravity = 1.0; ent->bounce = 0.0; ent->floating = false; }; \
  static void name##_collideent(entity_t *self, entity_t *other) {      \
  if(other->is_player && other->onpickup != NULL) {                     \
    if(self->target != NULL && self->target->ontrigger != NULL) {       \
      self->target->ontrigger(self->target, other);                     \
    }                                                                   \
    if(other->inventory[item] > 0) {                                    \
    hud_log(txt2);                                                       \
                                   } else {                             \
 hud_log(txt);                                                          \
                                           }                            \
    other->onpickup(other, item, amt);                                  \
    game_state.level_stats.items++;                                     \
    level_remove_entity(game_state.level, self);                       \
  }                                                                     \
    }                                                                   \
static void name##_init(entity_t *ent) {ent->onspawn = name##_spawn; ent->oncollideent = name##_collideent; };

CREATE_PICKUP(item_health, INVENTORY_HEALTH, 25, "YOU GOT A HEALTH PACK", "YOU GOT A HEALTH PACK");
CREATE_PICKUP(item_armor, INVENTORY_ARMOR, 50, "YOU GOT AN ARMOR PACK", "YOU GOT AN ARMOR PACK");
CREATE_PICKUP(item_shells, INVENTORY_SHELLS, 10, "YOU GOT SOME SHELLS", "YOU GOT SOME SHELLS");
CREATE_PICKUP(item_bullets, INVENTORY_BULLETS, 30, "YOU GOT A BOX OF BULLETS", "YOU GOT A BOX OF BULLETS");
CREATE_PICKUP(item_rockets, INVENTORY_ROCKETS, 5, "YOU GOT SOME ROCKETS", "YOU GOT SOME ROCKETS");
CREATE_PICKUP(item_shotgun, INVENTORY_SHOTGUN, 1, "YOU GOT THE SHOTGUN!", "YOU GOT ANOTHER SHOTGUN.");
CREATE_PICKUP(item_supershotgun, INVENTORY_SUPERSHOTGUN, 1, "YOU GOT THE SUPERSHOTGUN!", "YOU GOT ANOTHER SUPERSHOTGUN.");
CREATE_PICKUP(item_minigun, INVENTORY_MINIGUN, 1, "YOU GOT THE MINIGUN!", "YOU GOT ANOTHER MINIGUN.");
CREATE_PICKUP(item_rocketlauncher, INVENTORY_ROCKETLAUNCHER, 1, "YOU GOT THE ROCKETLAUNCHER!", "YOU GOT ANOTHER ROCKETLAUNCHER.");

/* decorations */

#define CREATE_DECORATION(name, r, h) static void name##_spawn(entity_t *ent) { ent->height = h; ent->radius = r; sprite_load("sprites/" #name ".png", &ent->sprite); ent->collision_type = ENT_COLLISION_TYPE_STATIC; }; \
  static void name##_init(entity_t *ent) {ent->onspawn = name##_spawn; };

CREATE_DECORATION(prop_lamppost, 0.4, 0.6);

#include "enemies/grunt.c"
#include "enemies/flinger.c"
#include "enemies/gunner.c"
#include "enemies/floateye.c"

/* entity spawnfuncs */

void spawn_entity(entity_t *ent) {
#define SPAWNFUNC(name) if(!strcmp(ent->classname, #name)) { name##_init(ent); return; }
  SPAWNFUNC(player);
  SPAWNFUNC(rocket);
  SPAWNFUNC(sparks);
  SPAWNFUNC(explosion);
  SPAWNFUNC(gib);
  SPAWNFUNC(casing);
  SPAWNFUNC(prop_barrel);
  SPAWNFUNC(prop_lamppost);
  SPAWNFUNC(item_health);
  SPAWNFUNC(item_armor);
  SPAWNFUNC(item_shells);
  SPAWNFUNC(item_bullets);
  SPAWNFUNC(item_rockets);
  SPAWNFUNC(item_shotgun);
  SPAWNFUNC(item_supershotgun);
  SPAWNFUNC(item_minigun);
  SPAWNFUNC(item_rocketlauncher);
  SPAWNFUNC(item_key);
  SPAWNFUNC(item_collectible);
  SPAWNFUNC(enemy_grunt);
  SPAWNFUNC(enemy_flinger);
  SPAWNFUNC(enemy_gunner);
  SPAWNFUNC(enemy_floateye);
  SPAWNFUNC(flinger_projectile);
  SPAWNFUNC(floateye_projectile);
  SPAWNFUNC(func_door);
  SPAWNFUNC(func_switch);

  SPAWNFUNC(trigger_changelevel);
  SPAWNFUNC(trigger_cylinder);
  SPAWNFUNC(trigger_teleport_destination);
  SPAWNFUNC(trigger_counter);
#undef SPAWNFUNC

  printf("Unknown classname %s\n", ent->classname);
}
