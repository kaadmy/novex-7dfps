
#include "local.h"
#include "util.h"

// Vector 3 functions

vec_t util_vec3_dot(const vec3_t a, const vec3_t b) {
  return (a[0] * b[0]) + (a[1] * b[1]) + (a[2] * b[2]);
}

vec_t util_vec3_length(const vec3_t a) {
  return sqrt(util_vec3_dot(a, a));
}

vec_t util_vec3_distance(const vec3_t a, const vec3_t b) {
  vec3_t v;
  UTIL_VEC3_SUB(v, a, b);

  return util_vec3_length(v);
}

vec_t util_vec3_normalize(vec3_t a) {
  vec_t length = util_vec3_length(a);

  if(length == 0.0) {
    UTIL_VEC3_SET(a, 0.0);

    return 1.0;
  }

  a[0] /= length;
  a[1] /= length;
  a[2] /= length;

  return length;
}

void util_vec3_clamp_aabb(vec3_t dest, const aabb_t aabb, const vec3_t src) {
  dest[0] = UTIL_CLAMP(src[0], aabb[0], aabb[3]);
  dest[1] = UTIL_CLAMP(src[1], aabb[1], aabb[4]);
  dest[2] = UTIL_CLAMP(src[2], aabb[2], aabb[5]);
}

vec_t util_vec3_nearest_distance_aabb(const aabb_t aabb, const vec3_t position) {
  vec3_t newpos;

  util_vec3_clamp_aabb(newpos, aabb, position);

  return util_vec3_distance(position, newpos);
}
