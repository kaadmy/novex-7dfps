/** entitydef: enemy_flinger **/

static void enemy_flinger_spawn(entity_t *ent) {
  sprite_load("sprites/enemy_flinger.png", &ent->sprite);
  ent->sprite.hframes = 9;
  ent->spawn_gibs = true;
  ent->gravity = 0.4;
  ent->state[0] = 2.0;
  ent->state[1] = 0.0;
  ent->state[2] = 0.0;
  ent->collision_type = ENT_COLLISION_TYPE_DYNAMIC;
  ent->radius = 0.4;
  ent->height = 1.2;
  ent->bounce = 0.0;
  ent->friction = 0.05;
  ent->mass = 200;
  ent->team = TEAM_ENEMY;
  ent->inventory[INVENTORY_HEALTH] = 30;

  game_state.level_stats.kills_total++;
}

static void enemy_flinger_update(entity_t *self) {
  if(self->target != NULL && self->target->remove) {
    self->target = NULL;
  }
  if(self->target == NULL) {
    for(int i = 0; i < game_state.level->n_entities; i++) {
      entity_t *ent = game_state.level->entities[i];
      if(!allow_target(self, ent)) {
        continue;
      }
      sound_play(game_state.sounds.grunt_wake, self->position[0], self->position[1], self->position[2] + 1);
      self->target = ent;
    }
  }
  if(!allow_target(self, self->target)) {
    self->target = NULL;
  }
  vec_t orig_time = self->state[0];
  if(self->target != NULL) {
    self->state[0] -= game_state.time_delta;
    self->state[2] -= game_state.time_delta;
    if(self->state[0] < 0.8) {
      self->sprite.frame = 2;
      if(self->state[0] < 0.5) {
        self->sprite.frame = 3;
      }
      if(self->state[0] < 0.2) {
        self->sprite.frame = 4;
      }
      if(orig_time >= 0.8) {
        sound_play(game_state.sounds.fireball, self->position[0], self->position[1], self->position[2] + 1);
      }
      vec3_t towards;
      UTIL_VEC3_SUB(towards, self->target->position, self->position);
      self->angle = atan2(-towards[0], towards[1]);
      if(self->state[0] < 0.2 && orig_time >= 0.2) {
        vec4_t vars = {};
        vec3_t pos;
        UTIL_VEC3_COPY(pos, self->position);
        pos[2] += 0.5;
        entity_t *projectile = entity_init(game_state.level, "flinger_projectile", pos, self->angle, false, false, vars);
        projectile->owner = self;
        level_add_entity(game_state.level, projectile);
      }
      if(self->state[0] < 0.0) {
        self->state[1] = (rand() % 128) / 128.0;
        if(self->state[1] > 0.6) {
          self->angle = ((rand() % 128) / 128.0) * M_PI * 2;
        }
        self->state[0] = ((rand() % 6) / 3.0) + 2.6;
        self->sprite.animation_time = rand() % 2 ? 0.2 : 0;
      }
    } else {
      while(self->sprite.animation_time > 0.2) {
        self->sprite.animation_time -= 0.2;
        self->sprite.frame += 1;
        if(self->sprite.frame > 1) {
          self->sprite.frame = 0;
        }
      }
      self->sprite.animation_time += game_state.time_delta;
      vec3_t forward = {-sin(self->angle), cos(self->angle), 0};
      vec_t spd = 1.6;
      UTIL_VEC3_MA(forward, forward, spd - 1);
      UTIL_VEC3_MA(self->velocity, forward, game_state.time_delta);
    }
  }
}

static void enemy_flinger_update_dead(entity_t *self) {
  while(self->sprite.animation_time > 0.2) {
    self->sprite.animation_time -= 0.2;
    self->sprite.frame += 1;
  }
  if(self->sprite.frame >= 8) {
    self->sprite.frame = 8;
  } else {
    self->sprite.animation_time += game_state.time_delta;
  }
}

static void enemy_flinger_damage(entity_t *self, entity_t *other, vec_t dmg) {
  self->inventory[INVENTORY_HEALTH] -= dmg;

  if(game_state.jodmode) {
    self->inventory[INVENTORY_HEALTH] = -1;
  }

  vec3_t pos;
  UTIL_VEC3_COPY(pos, self->position);
  pos[2] += 0.8;
  if(self->inventory[INVENTORY_HEALTH] <= 0) {
    if(self->target != NULL && self->target->ontrigger != NULL) {
      self->target->ontrigger(self->target, other);
    }
    game_state.level_stats.kills++;
    self->collision_type = ENT_COLLISION_TYPE_CORPSE;
    self->team = TEAM_NEUTRAL;
    self->sprite.animation_time = 0.0;
    self->ondamage = NULL;
    self->onupdate = &enemy_flinger_update_dead;
    self->state[0] = 0;
    self->sprite.frame = 5;
    spawn_gibs(pos, 4);
    //level_remove_entity(game_state.level, self);
  }
  self->state[2] = 0.5;
  if(allow_infighting(self, other)) {
    self->target = other;
  }
}

static void enemy_flinger_init(entity_t *ent) {
  ent->onspawn = enemy_flinger_spawn;
  ent->onupdate = enemy_flinger_update;
  ent->ondamage = enemy_flinger_damage;
}

/** entitydef: flinger_projectile **/

static void flinger_projectile_update(entity_t *ent) {
  if(ent->state[0] > 0.03) {
    vec4_t vars = {};
    entity_t *sparks = entity_init(game_state.level, "sparks", ent->position, 0.0, false, false, vars);
    level_add_entity(game_state.level, sparks);
    ent->state[0] = 0;
  }
  ent->state[0] += game_state.time_delta;
  if(ent->state[1] > 3) {
    level_remove_entity(game_state.level, ent);
  }
  ent->state[1] += game_state.time_delta;
}

static void flinger_projectile_spawn(entity_t *ent) {
  sprite_load("sprites/object_flinger_projectile.png", &ent->sprite);
  ent->collision_type = ENT_COLLISION_TYPE_TRIGGER;
  ent->z_offset = 3;
  ent->radius = 0.3;
  ent->height = 0.3;
  ent->floating = true;
  ent->velocity[0] = -sin(ent->angle) * 10.0;
  ent->velocity[1] = cos(ent->angle) * 10.0;
  ent->velocity[2] = autoaim(ent->owner, 0.5) * 10.0;
  ent->friction = 1.0;
  ent->inventory[INVENTORY_HEALTH] = 1;
}

static void flinger_projectile_collideent(struct entity_s *self, struct entity_s *other) {
  if(self->inventory[INVENTORY_HEALTH] > 0) {
    if(other != self->owner) {
      self->inventory[INVENTORY_HEALTH] = 0;
      vec3_t zero = {};
      spawn_explosion(self->owner, self->position, 22.0, 0.8, zero);
      level_remove_entity(game_state.level, self);
    }
  }
}

static void flinger_projectile_collidetri(struct entity_s *self, level_triangle_t *other, vec3_t normal) {
  if(self->inventory[INVENTORY_HEALTH] > 0) {
    spawn_explosion(self, self->position, 22.0, 0.8, normal);
    level_remove_entity(game_state.level, self);
    self->inventory[INVENTORY_HEALTH] = 0;
  }
}

static void flinger_projectile_init(entity_t *ent) {
  ent->onspawn = flinger_projectile_spawn;
  ent->onupdate = flinger_projectile_update;
  ent->oncollideent = flinger_projectile_collideent;
  ent->oncollidetri = flinger_projectile_collidetri;

}
