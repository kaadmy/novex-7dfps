/** entitydef: enemy_grunt **/

static void enemy_grunt_spawn(entity_t *ent) {
  sprite_load("sprites/enemy_grunt.png", &ent->sprite);
  ent->sprite.hframes = 9;
  ent->spawn_gibs = true;
  ent->gravity = 0.4;
  ent->state[0] = 2.0;
  ent->state[1] = 0.0;
  ent->state[2] = 0.0;
  ent->collision_type = ENT_COLLISION_TYPE_DYNAMIC;
  ent->radius = 0.3;
  ent->height = 1.2;
  ent->bounce = 0.0;
  ent->friction = 0.05;
  ent->mass = 140;
  ent->team = TEAM_ENEMY;
  ent->inventory[INVENTORY_HEALTH] = 20;

  game_state.level_stats.kills_total++;
}

static void enemy_grunt_update(entity_t *self) {
  if(self->target != NULL && self->target->remove) {
    self->target = NULL;
  }
  if(self->target == NULL) {
    for(int i = 0; i < game_state.level->n_entities; i++) {
      entity_t *ent = game_state.level->entities[i];
      if(ent == self) {
        continue;
      }
      if(!allow_target(ent, self)) {
        continue;
      }
      sound_play(game_state.sounds.grunt_wake, self->position[0], self->position[1], self->position[2] + 1);
      self->target = ent;
    }
  }
  if(!allow_target(self, self->target)) {
    self->target = NULL;
  }
  vec_t orig_time = self->state[0];
  if(self->target != NULL) {
    self->state[0] -= game_state.time_delta;
    self->state[2] -= game_state.time_delta;
    if(self->state[0] < 0.6) {
      self->sprite.frame = 2;
      if(self->state[0] < 0.4) {
        self->sprite.frame = 3;
      }
      if(self->state[0] < 0.2) {
        self->sprite.frame = 4;
      }
      if(orig_time >= 0.6) {
        vec3_t towards;
        UTIL_VEC3_SUB(towards, self->target->position, self->position);
        self->angle = atan2(-towards[0], towards[1]);
      }
      if(self->state[0] < 0.4 && orig_time >= 0.4) {
        for(int i = 0; i < 6; i++) {
          shoot_bullet(4.5, 3.0, self);
        }
        sound_play(game_state.sounds.grunt_fire, self->position[0], self->position[1], self->position[2] + 1);
      }
      if(self->state[0] < 0.0) {
        self->state[1] = (rand() % 128) / 128.0;
        if(self->state[1] > 0.6) {
          self->angle = ((rand() % 128) / 128.0) * M_PI * 2;
        }
        self->state[0] = ((rand() % 6) / 8.0) + 0.6;
        self->sprite.animation_time = rand() % 2 ? 0.2 : 0;
      }
    } else {
      while(self->sprite.animation_time > 0.2) {
        self->sprite.animation_time -= 0.2;
        self->sprite.frame += 1;
        if(self->sprite.frame > 1) {
          self->sprite.frame = 0;
        }
      }
      self->sprite.animation_time += game_state.time_delta;
      vec3_t forward = {-sin(self->angle), cos(self->angle), 0};
      vec_t spd = 1.6;
      UTIL_VEC3_MA(forward, forward, spd - 1);
      UTIL_VEC3_MA(self->velocity, forward, game_state.time_delta);
    }
  }
}

static void enemy_grunt_update_dead(entity_t *self) {
  while(self->sprite.animation_time > 0.2) {
    self->sprite.animation_time -= 0.2;
    self->sprite.frame += 1;
  }
  if(self->sprite.frame >= 8) {
    self->sprite.frame = 8;
  } else {
    self->sprite.animation_time += game_state.time_delta;
  }
}

static void enemy_grunt_damage(entity_t *self, entity_t *other, vec_t dmg) {
  self->inventory[INVENTORY_HEALTH] -= dmg;

  if(game_state.jodmode) {
    self->inventory[INVENTORY_HEALTH] = -1;
  }

  vec3_t pos;
  UTIL_VEC3_COPY(pos, self->position);
  pos[2] += 0.8;
  if(self->inventory[INVENTORY_HEALTH] <= 0) {
    if(self->target != NULL && self->target->ontrigger != NULL) {
      self->target->ontrigger(self->target, other);
    }
    game_state.level_stats.kills++;
    vec3_t pos;
    UTIL_VEC3_COPY(pos, self->position);
    pos[2] += 0.7;
    vec4_t vars = {};
    entity_t *weapon = entity_init(game_state.level, "item_shotgun", pos, 0.0, false, false, vars);
    weapon->velocity[0] = (((rand() % 128) / 128.0) - 0.5) * 6.0;
    weapon->velocity[1] = (((rand() % 128) / 128.0) - 0.5) * 6.0;
    weapon->velocity[2] = (((rand() % 128) / 128.0) - 0.5) * 4.0 + 6.0;
    level_add_entity(game_state.level, weapon);

    self->collision_type = ENT_COLLISION_TYPE_CORPSE;
    self->team = TEAM_NEUTRAL;
    self->sprite.animation_time = 0.0;
    self->ondamage = NULL;
    self->onupdate = &enemy_grunt_update_dead;
    self->state[0] = 0;
    self->sprite.frame = 5;
    spawn_gibs(pos, 4);
    //level_remove_entity(game_state.level, self);
  }
  self->state[2] = 0.5;
  if(allow_infighting(self, other)) {
    self->target = other;
  }
}

static void enemy_grunt_init(entity_t *ent) {
  ent->onspawn = enemy_grunt_spawn;
  ent->onupdate = enemy_grunt_update;
  ent->ondamage = enemy_grunt_damage;
}
