
#include "local.h"

/* renderer init */

void renderer_init(void) {
  glClearColor(0.2, 0.2, 0.2, 1.0);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glEnable(GL_TEXTURE_2D);

  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GREATER, 0.5);
}

/* render a single frame (called from game_update) */

void renderer_render(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glViewport(0, 0, global_state.window_size[0], global_state.window_size[1]);

  game_render();
}
