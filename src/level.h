
#pragma once

level_t *level_init(const char *path);
void level_free(level_t *level);
void level_add_entity(level_t *level, entity_t *e);
void level_remove_entity(level_t *level, entity_t *e);
void level_update(level_t *level);
void level_render(level_t *level);
void level_render_sky(level_t *level);
bool level_get_floor_ceiling(level_t *level, bool ceiling, vec3_t pos, vec_t *out_height, level_triangle_t **out_triangle);
void level_collide_walls(level_t *level, entity_t *ent, vec3_t pos, vec_t radius, float z_offset, void (*oncollide)(struct entity_s *self, level_triangle_t *other, vec3_t normal));
level_triangle_t *level_cast_ray(level_t *level, vec3_t start, vec3_t dir, vec3_t *out_position, vec_t *out_distance);
entity_t *level_cast_ray_ent(level_t *level, vec3_t start, vec3_t dir, vec3_t *out_position, vec_t *out_distance, entity_t *ignore);
entity_t *level_cast_ray_ent_no_z(level_t *level, vec3_t start, vec3_t dir, vec3_t *out_position, vec_t *out_distance, entity_t *ignore);

int level_get_material(level_t *level, const char *name);

// returns where the original triangle is copied to
level_triangle_t *level_add_triangle(level_t *level, level_triangle_t *tri);
