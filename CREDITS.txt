
# Assets

- data/sounds/shotgun_fire.ogg: CC-BY-SA 3.0 by Q009
- data/sounds/minigun_fire.ogg: CC-BY-SA 3.0 by Q009
- data/sounds/rocketlauncher_fire.ogg: CC-BY-SA 3.0 by Q009
- data/sounds/grunt_fire.ogg: CC-BY-SA 3.0 by Q009
- data/sounds/explosion.ogg: CC-BY-SA 3.0 by Q009
- data/sounds/pickup_item.ogg: CC-BY-SA 3.0 by Q009
- data/sounds/pickup_weapon.ogg (modified): CC-BY-SA 3.0 by Q009

Link: https://opengameart.org/content/q009s-weapon-sounds

---

- data/sounds/teleport.ogg: CC0 by fins

Link: https://freesound.org/people/fins/sounds/172206/

---

- data/sounds/fireball.ogg: CC0 by Julien Matthey

Link: https://freesound.org/people/Julien%20Matthey/sounds/105016/

---

- data/sounds/door_lock.ogg: CC0 by KaadmY
- data/sounds/door_move.ogg: CC0 by KaadmY
- data/sounds/grunt_idle.ogg: CC0 by KaadmY
- data/sounds/grunt_wake.ogg: CC0 by KaadmY
- data/sounds/hump.ogg: CC0 by KaadmY
- data/sounds/pickup_armor.ogg: CC0 by KaadmY
- data/sounds/pickup_health.ogg: CC0 by KaadmY
- data/sounds/pickup_key.ogg: CC0 by KaadmY
- data/sounds/switch_activate.ogg: CC0 by KaadmY
- data/sounds/switch_locked.ogg: CC0 by KaadmY
- data/sounds/switch_return.ogg: CC0 by KaadmY

---

- data/music/S31-Futuristic-Resources.ogg: CC0 by Section 31 - Tech

Link: https://opengameart.org/content/futuristic-resources

---

- data/levels/*: CC0 by KaadmY and LiterallyVoid
- data/sprites/*: CC0 by KaadmY
- data/tiles/*: CC0 by KaadmY and LiterallyVoid

---

- src/stb_image.h: MIT by Sean "nothings" Barett

Link: https://github.com/nothings/stb/blob/master/stb_image.h

---

- src/entity.c: MIT by KaadmY and LiterallyVoid
- src/game.c: MIT by KaadmY and LiterallyVoid
- src/glad.c: MIT by KaadmY and LiterallyVoid
- src/hud.c: MIT by KaadmY and LiterallyVoid
- src/level.c: MIT by KaadmY and LiterallyVoid
- src/main.c: MIT by KaadmY and LiterallyVoid
- src/renderer.c: MIT by KaadmY and LiterallyVoid
- src/sound.c: MIT by KaadmY and LiterallyVoid
- src/spawn.c: MIT by KaadmY and LiterallyVoid
- src/sprite.c: MIT by KaadmY and LiterallyVoid
- src/texture.c: MIT by KaadmY and LiterallyVoid
- src/util.c: MIT by KaadmY and LiterallyVoid
- src/config.h: MIT by KaadmY and LiterallyVoid
- src/entity.h: MIT by KaadmY and LiterallyVoid
- src/game.h: MIT by KaadmY and LiterallyVoid
- src/glad.h: MIT by KaadmY and LiterallyVoid
- src/hud.h: MIT by KaadmY and LiterallyVoid
- src/level.h: MIT by KaadmY and LiterallyVoid
- src/local.h: MIT by KaadmY and LiterallyVoid
- src/main.h: MIT by KaadmY and LiterallyVoid
- src/renderer.h: MIT by KaadmY and LiterallyVoid
- src/sound.h: MIT by KaadmY and LiterallyVoid
- src/spawn.h: MIT by KaadmY and LiterallyVoid
- src/sprite.h: MIT by KaadmY and LiterallyVoid
- src/texture.h: MIT by KaadmY and LiterallyVoid
- src/types.h: MIT by KaadmY and LiterallyVoid
- src/util.h: MIT by KaadmY and LiterallyVoid
- src/enemies/flinger.c: MIT by KaadmY and LiterallyVoid
- src/enemies/floateye.c: MIT by KaadmY and LiterallyVoid
- src/enemies/grunt.c: MIT by KaadmY and LiterallyVoid
- src/enemies/gunner.c: MIT by KaadmY and LiterallyVoid

# Author Links

- KaadmY: https://kaadmy.itch.io/
- LiterallyVoid: https://literallyvoid.itch.io/
- Section 31 - Tech: https://s31tech.org/
- Sean "nothings" Barret: https://twitter.com/nothings
- fins: https://freesound.org/people/fins/
- Julien Matthey: https://freesound.org/people/Julien%20Matthey/
